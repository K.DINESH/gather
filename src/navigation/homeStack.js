import React, {useContext} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {ConversationsScreen} from '../screens/conversationsScreen';
import {LogInScreen} from '../screens/loginScreens/loginScreen';
import {AppContext} from '../context/useContext';
import {ConvertionHeader} from '../components/headers/convertionHeader';
import {AddParticipantsHeader} from '../components/headers/addParticipantsHeader';
import {CreateGroupScreen} from '../screens/createGroupScreen';
import {ThisWeekSparkJoingScreen} from '../screens/thisWeekSparkJoiningScreen';
import {ThisWeekSparkEditingScreen} from '../screens/thisWeekSparkEditingScreen';
import {ThisWeekSparkEditingHeader} from '../components/headers/thisWeekSparkEditingHeader';
import {AddParticipantsScreen} from '../screens/addParticipantsScreen';
import {SharingScreen} from '../screens/sharingScreen';
import {CommonHeader} from '../components/headers/commonHaeder';
import {SharingHeader} from '../components/headers/sharingHeader';
import {SourceCodeWebView} from '../screens/sourceCodeWebView';
import {CollegeBuds} from './collegeBudsTopNavigation';
import {CollegeBudsHeader} from '../components/headers/collegeBudsHeader';
import {RegistrationScreen} from '../screens/loginScreens/registerScreen';
import {SendLoveScreen} from '../screens/collegeBudsScreens/sendLoveScreen';

const Stack = createStackNavigator();

const commonHeader =
  title =>
  ({navigation}) => ({
    header: () => <CommonHeader navigation={navigation} title={title} />,
  });

const screens = [
  {
    name: 'login',
    component: LogInScreen,
    options: {headerShown: false},
  },
  {
    name: 'registrationScreen',
    component: RegistrationScreen,
    options: {headerShown: false},
  },

  {
    name: 'conversationScreen',
    component: ConversationsScreen,
    options: ({navigation}) => ({
      header: () => <ConvertionHeader navigation={navigation} />,
    }),
  },
  {
    name: 'addParticipantsScreen',
    component: AddParticipantsScreen,
    options: ({navigation}) => ({
      header: props => (
        <AddParticipantsHeader
          navigation={navigation}
          selectedData={props?.route?.params?.selectedData}
        />
      ),
    }),
  },
  {
    name: 'createGroupScreen',
    component: CreateGroupScreen,
    options: commonHeader('Create Group'),
  },
  {
    name: 'thisWeekSparkHomeScreen',
    component: ThisWeekSparkJoingScreen,
    options: commonHeader('This Week Spark'),
  },
  {
    name: 'thisWeekSparkEditingScreen',
    component: ThisWeekSparkEditingScreen,
    options: ({navigation}) => ({
      header: props => (
        <ThisWeekSparkEditingHeader
          navigation={navigation}
          allFieldsFilled={props?.route?.params?.allFieldsFilled}
          answers={props?.route?.params?.answers}
        />
      ),
    }),
  },
  {
    name: 'sharingScreen',
    component: SharingScreen,
    options: ({navigation}) => ({
      header: () => <SharingHeader navigation={navigation} />,
    }),
  },
  {
    name: 'sourceCodeWebView',
    component: SourceCodeWebView,
    options: commonHeader('App Source Code'),
  },
  {
    name: 'collegeBuds',
    component: CollegeBuds,
    options: ({navigation}) => ({
      header: props => (
        <CollegeBudsHeader
          navigation={navigation}
          weeks={props?.route?.params?.groupDetails?.weeks}
        />
      ),
    }),
  },
  {
    name: 'sendLoveScreen',
    component: SendLoveScreen,
    options: commonHeader('Send Love'),
  },
];

export const HomeStack = () => {
  const {firebaseUserDetails} = useContext(AppContext);

  return (
    <Stack.Navigator
      initialRouteName={!firebaseUserDetails ? 'login' : 'conversationScreen'}>
      {screens.map((screenDetails, index) => {
        return (
          <Stack.Screen
            key={index}
            name={screenDetails?.name}
            component={screenDetails?.component}
            options={screenDetails?.options}
          />
        );
      })}
    </Stack.Navigator>
  );
};
