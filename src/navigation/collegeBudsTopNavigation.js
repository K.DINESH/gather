import React, {useContext} from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {GroupScreen} from '../screens/collegeBudsScreens/groupScreen';
import {AppContext} from '../context/useContext';
import {Text, View} from 'react-native';

const Tab = createMaterialTopTabNavigator();

const NewGroup = () => {
  return (
    <View>
      <Text>new group</Text>
    </View>
  );
};

export const CollegeBuds = ({route}) => {
  const {collegeBudsRef} = useContext(AppContext);
  const groupDetails = route?.params?.groupDetails;
  const weeks = groupDetails?.weeks;

  return (
    <Tab.Navigator
      screenOptions={({navigation}) => {
        collegeBudsRef.current = navigation;
        return {
          tabBarStyle: {display: 'none'},
        };
      }}>
      {weeks ? (
        Object.keys(weeks).map((id, index) => {
          return (
            <Tab.Screen
              name={id}
              component={GroupScreen}
              options={{headerShown: false}}
              initialParams={{
                groupDetails,
                sparkId: id,
              }}
              key={index}
            />
          );
        })
      ) : (
        <Tab.Screen
          name={'newGroup'}
          component={NewGroup}
          options={{headerShown: false}}
        />
      )}
    </Tab.Navigator>
  );
};
