import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {GroupScreen} from '../screens/collegeBudsScreens/groupScreen';
import {SendLoveScreen} from '../screens/collegeBudsScreens/sendLoveScreen';

const Stack = createStackNavigator();

// const screens = [
//   {
//     name: 'login',
//     component: LogInScreen,
//     options: {headerShown: false},
//   },
// ];

export const CollegeBudsStackNavigation = ({route}) => {
  const {groupDetails, sparkId} = route?.params;
  return (
    <Stack.Navigator>
      <Stack.Screen
        name={'logInScreen'}
        component={GroupScreen}
        options={{headerShown: false}}
        initialParams={{groupDetails: {...groupDetails}, sparkId: sparkId}}
      />
      <Stack.Screen
        name={'sendLoveScreen'}
        component={SendLoveScreen}
        options={{headerShown: false}}
        initialParams={{groupDetails: {...groupDetails}, sparkId: sparkId}}
      />
    </Stack.Navigator>
  );
};
