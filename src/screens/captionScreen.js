import React, {useState} from 'react';
import {
  Dimensions,
  Image,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {FontUtils} from '../utilities/fontutilities';

const screenHeight = Dimensions.get('screen').height;

export const CaptionScreen = ({uri, setModalVisible}) => {
  const [caption, setCaption] = useState('');
  return (
    <View style={styles.scrollView}>
      <Image source={{uri}} resizeMode="contain" style={styles.image} />
      <TouchableOpacity
        onPress={() => setModalVisible(false)}
        style={styles.crossImageView}>
        <Image
          source={require('../assets/images/cross.png')}
          style={styles.crossImage}
        />
      </TouchableOpacity>
      <View style={styles.accessoryView}>
        <TextInput
          style={styles.accessoryViewTextInput}
          placeholder="Add a caption"
          value={caption}
          onChangeText={setCaption}
        />
        <TouchableOpacity onPress={() => setModalVisible(false)}>
          <Image
            source={require('../assets/images/send.png')}
            style={styles.sendImage}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: 'black',
  },
  image: {
    height: screenHeight,
    width: '100%',
  },
  crossImageView: {
    height: 24,
    width: 24,
    position: 'absolute',
    left: 25,
    top: 23,
  },
  crossImage: {
    height: '100%',
    width: '100%',
    tintColor: 'white',
  },
  accessoryView: {
    position: 'absolute',
    bottom: 22,
    left: 20,
    right: 20,
    flexDirection: 'row',
    alignItems: 'center',
  },
  accessoryViewTextInput: {
    paddingVertical: 6,
    paddingHorizontal: 14,
    ...FontUtils.font({
      fontSize: 12,
      color: 'rgb(155, 155, 155)',
      letterSpacing: 0.75,
      fontFamily: 'Raleway-SemiBold',
    }),
    flex: 1,
    marginRight: 10,
    borderRadius: 25,
    backgroundColor: 'white',
    shadowColor: 'rgba( 0, 0, 0, 0.35)',
    elevation: 10,
  },
  sendImage: {
    width: 44,
    height: 44,
  },
});
