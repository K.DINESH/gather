import React, {useContext, useEffect, useMemo, useState} from 'react';
import {
  FlatList,
  Image,
  SectionList,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import {SearchBar} from '../components/searchBar';
import {SelectButton} from '../components/selectButton';
import {AppContext} from '../context/useContext';
import {FontUtils, structuringContacts} from '../utilities/fontutilities';

const pushOrPopSelectedData = ({
  item,
  index,
  section,
  setSelectedData,
  setSearchedContacts,
  callFrom = 'sectionList',
}) => {
  let changedItem = {};
  let sectionIndex;

  setSearchedContacts(prv => {
    if (callFrom === 'sectionList') sectionIndex = prv.indexOf(section);
    else sectionIndex = section;
    changedItem = {
      ...item,
      isclicked: item?.isclicked ? false : true,
    };
    prv[sectionIndex].data[index] = changedItem;
    return [...prv];
  });

  setSelectedData(prv => {
    let data;
    data = prv.filter(value => {
      return (
        JSON.stringify(value) !==
        JSON.stringify({sectionIndex, data: item, index})
      );
    });

    if (prv.length === data.length)
      data.push({sectionIndex, data: changedItem, index});
    return [...data];
  });
};

const ProfileImage = ({style, item}) => {
  return item?.thumbnailPath?.length === 0 ? (
    <View
      style={{
        ...style,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <Text style={styles.profileImageText}>{item?.displayName?.[0]}</Text>
    </View>
  ) : (
    <Image style={style} source={{uri: item?.thumbnailPath}} />
  );
};

const ListOfContacts =
  (setSearchedContacts, setSelectedData) =>
  ({item, index, section}) => {
    const hideBorder = section.data.length < index + 2;
    return (
      <TouchableOpacity
        style={{
          ...styles.contactView,
          borderBottomWidth: hideBorder ? 0 : 1,
        }}
        onPress={() =>
          pushOrPopSelectedData({
            item,
            index,
            section,
            setSelectedData,
            setSearchedContacts,
          })
        }>
        <View style={styles.profileImageAndName}>
          <ProfileImage style={styles.profileImage} item={item} />
          <Text style={styles.displayName}>{item?.displayName}</Text>
        </View>
        <SelectButton select={item?.isclicked} />
      </TouchableOpacity>
    );
  };

const selectedContacts =
  (setSelectedData, setSearchedContacts) =>
  ({item}) => {
    return (
      <View style={styles.selectedDataView}>
        <View>
          <ProfileImage style={styles.selectedProfileImage} item={item.data} />
          <TouchableOpacity
            style={styles.selectedCrossImageView}
            onPress={() =>
              pushOrPopSelectedData({
                item: item.data,
                index: item.index,
                setSelectedData,
                setSearchedContacts,
                callFrom: 'flatList',
                section: item.sectionIndex,
              })
            }>
            <Image
              style={styles.selectedCrossImage}
              source={require('../assets/images/imageCross.png')}
            />
          </TouchableOpacity>
        </View>
        <Text style={styles.selectedDisplayName}>
          {item?.data?.displayName}
        </Text>
      </View>
    );
  };

export const AddParticipantsScreen = ({navigation}) => {
  const {sortedContacts} = useContext(AppContext);
  const [searchedContacts, setSearchedContacts] = useState(
    structuringContacts(sortedContacts),
  );
  const [selectedData, setSelectedData] = useState([]);

  useEffect(() => {
    navigation.setParams({selectedData: selectedData});
  }, [selectedData]);

  const sectionList = useMemo(() => {
    return (
      <SectionList
        sections={searchedContacts}
        renderItem={ListOfContacts(setSearchedContacts, setSelectedData)}
        renderSectionHeader={({section}) => (
          <Text style={styles.separatorText}>{section.title}</Text>
        )}
        stickySectionHeadersEnabled
        keyExtractor={(item, index) => item?.recordID + index}
      />
    );
  }, [searchedContacts]);

  const selectedDataFlatList = useMemo(() => {
    return (
      <View style={styles.selectedFlatList}>
        <FlatList
          horizontal
          data={selectedData}
          renderItem={selectedContacts(setSelectedData, setSearchedContacts)}
        />
      </View>
    );
  }, [selectedData]);

  return (
    <View style={styles.container}>
      <View style={styles.searchBarBackgroundView}>
        <SearchBar setSearchedContacts={setSearchedContacts} />
      </View>

      {selectedData?.length != 0 && selectedDataFlatList}

      {sectionList}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  searchBarBackgroundView: {
    backgroundColor: 'rgba(6, 51, 164, 0.1)',
    paddingVertical: 7,
    paddingHorizontal: 8,
  },
  selectedFlatList: {
    paddingHorizontal: 13,
    paddingVertical: 9,
  },
  selectedDataView: {
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
  },
  selectedProfileImage: {
    width: 55,
    height: 55,
    backgroundColor: 'pink',
    borderRadius: 40,
  },
  selectedCrossImageView: {
    position: 'absolute',
    right: 0,
    top: 0,
    width: 25,
    height: 25,
  },
  selectedCrossImage: {
    alignSelf: 'flex-end',
    width: 15,
    height: 15,
  },
  selectedDisplayName: {
    ...FontUtils.font({
      color: 'black',
      fontSize: 12,
      letterSpacing: 0,
    }),
  },
  contactView: {
    marginHorizontal: 15,
    borderBottomColor: 'lightgrey',
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  contactsSeparatorView: {
    backgroundColor: 'lightgrey',
    paddingHorizontal: 15,
    justifyContent: 'center',
    paddingVertical: 5,
  },
  separatorText: {
    ...FontUtils.font({
      color: 'black',
      fontSize: 18,
      letterSpacing: 0,
    }),
    backgroundColor: 'lightgrey',
    paddingHorizontal: 15,
    justifyContent: 'center',
    paddingVertical: 5,
  },
  profileImageAndName: {
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
  },
  profileImage: {
    width: 40,
    height: 40,
    backgroundColor: 'pink',
    borderRadius: 40,
  },
  profileImageText: {
    ...FontUtils.font({fontSize: 12, color: 'black', letterSpacing: 0}),
  },
  displayName: {
    ...FontUtils.font({
      color: 'black',
      fontSize: 18,
      letterSpacing: 0,
    }),
    marginLeft: 15,
  },
});
