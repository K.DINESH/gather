import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import LoadingIndicator from '../components/loadingModal';
import {SelectButton} from '../components/selectButton';
import {AppContext} from '../context/useContext';
import {
  FontUtils,
  selectAll,
  shareSparkToGroups,
} from '../utilities/fontutilities';

const pushOrPopSelectedData = ({
  item,
  setGroup_outerDetails,
  setSelectedGroupIds,
}) => {
  let changedItem = {};
  let data;
  setGroup_outerDetails(prv => {
    changedItem = {
      ...item,
      isclicked: item?.isclicked ? false : true,
    };
    prv[item.group_id] = changedItem;
    return {...prv};
  });

  setSelectedGroupIds(prv => {
    if (prv.includes(item.group_id)) {
      data = prv.filter(value => {
        return value !== item.group_id;
      });
    } else {
      data = [...prv];
      data.push(item.group_id);
    }
    return [...data];
  });
};

export const SharingScreen = ({navigation}) => {
  const {filledSpark, thisWeekSpark, userData} = useContext(AppContext);
  const [selectedGroupIds, setSelectedGroupIds] = useState([]);
  const [loader, setLoader] = useState(false);
  const [group_outerDetails, setGroup_outerDetails] = useState({});

  const listOfGroups = useCallback(
    ({item, index}) => {
      return (
        <TouchableOpacity
          style={styles.contactView}
          onPress={() =>
            pushOrPopSelectedData({
              item: group_outerDetails[item],
              index,
              setGroup_outerDetails,
              setSelectedGroupIds,
            })
          }>
          <View style={styles.profileImageAndName}>
            <Image
              style={styles.profileImage}
              source={{uri: group_outerDetails[item]?.imageUri}}
            />
            <Text style={styles.displayName}>
              {group_outerDetails[item]?.groupName}
            </Text>
          </View>
          <SelectButton select={group_outerDetails[item]?.isclicked} />
        </TouchableOpacity>
      );
    },
    [group_outerDetails],
  );

  const flatListHeader = useMemo(() => {
    return (
      <View style={styles.header}>
        <Text style={styles.existingText}>EXISTING CONVERSATIONS</Text>
        <TouchableOpacity
          onPress={() => selectAll(setGroup_outerDetails, setSelectedGroupIds)}>
          <Text style={styles.selectText}>Select All</Text>
        </TouchableOpacity>
      </View>
    );
  });

  useEffect(() => {
    const groupDetails = {};
    for (let key in userData.group_outerDetails) {
      if (
        !Object.keys(userData?.group_outerDetails?.[key]?.weeks || {}).includes(
          thisWeekSpark?.current?.id,
        )
      )
        groupDetails[key] = userData.group_outerDetails[key];
    }
    setGroup_outerDetails({...groupDetails});
  }, []);

  return (
    <View style={styles.container}>
      <LoadingIndicator visible={loader} />
      <FlatList
        style={styles.container}
        data={Object.keys(group_outerDetails)}
        renderItem={listOfGroups}
        ListHeaderComponent={flatListHeader}
        stickyHeaderIndices={[0]}
        keyExtractor={(item, index) =>
          group_outerDetails[item]?.groupName + index
        }
      />
      <View style={styles.footerView}>
        <TouchableOpacity
          onPress={() =>
            shareSparkToGroups({
              filledSpark,
              selectedGroupIds,
              navigation,
              thisWeekSpark,
              userData,
              setLoader,
            })
          }
          disabled={selectedGroupIds.length === 0}>
          <Text
            style={{
              ...styles.footerText,
              opacity: selectedGroupIds.length != 0 ? 1 : 0.5,
            }}>
            share
          </Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(250, 251, 253)',
  },
  contactView: {
    marginHorizontal: 15,
    borderBottomColor: 'lightgrey',
    paddingVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  profileImageAndName: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  profileImage: {
    width: 40,
    height: 40,
    borderRadius: 40,
  },
  displayName: {
    ...FontUtils.font({
      color: 'black',
      fontSize: 18,
      letterSpacing: 0,
    }),
    marginLeft: 15,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 6,
    paddingRight: 17,
    paddingVertical: 10,
    backgroundColor: 'rgb(250, 251, 253)',
  },
  existingText: {
    ...FontUtils.font({
      fontSize: 14,
      color: 'rgb(111, 111, 111)',
      letterSpacing: 0,
    }),
  },
  selectText: {
    ...FontUtils.font({
      fontSize: 16,
      color: 'rgb(74, 144, 226)',
      letterSpacing: 0,
      fontFamily: 'Raleway-Medium',
    }),
  },
  footerView: {
    paddingVertical: 14,
    paddingRight: 20,
    backgroundColor: 'rgb(151 ,151, 151)',
    alignItems: 'flex-end',
  },
  footerText: {
    ...FontUtils.font({
      fontSize: 20,
      letterSpacing: 0,
      color: 'white',
      fontFamily: 'Raleway-Light',
    }),
  },
});
