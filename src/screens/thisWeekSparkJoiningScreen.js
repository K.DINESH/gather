import {StyleSheet, ScrollView} from 'react-native';
import React from 'react';

import {ThisWeekSpark} from '../components/thisWeekSpark';
export function ThisWeekSparkJoingScreen({navigation}) {
  return (
    <ScrollView style={styles.scrollView}>
      <ThisWeekSpark navigation={navigation} />
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: 'white',
  },
});
