import React, {useContext, useEffect, useMemo, useRef, useState} from 'react';
import {
  FlatList,
  Image,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {
  checkContactPermission,
  fetchDataFromFirebase,
  FontUtils,
} from '../utilities/fontutilities';

import {AskPermissions} from '../components/askPermission';
import {AppContext} from '../context/useContext';
import LoadingIndicator from '../components/loadingModal';

const groups =
  (navigateToGroupsScreen, userData) =>
  ({item}) => {
    return (
      <TouchableOpacity
        style={styles.groupOrUserView}
        onPress={() =>
          navigateToGroupsScreen(userData?.group_outerDetails[item].group_id)
        }>
        <View style={styles.imageAndNameView}>
          <Image
            style={styles.groupOrUserImage}
            source={{uri: userData?.group_outerDetails[item]?.imageUri}}
          />
          <View style={styles.groupOrUserNameAndStatus}>
            <Text style={styles.groupOrUserName}>
              {userData?.group_outerDetails?.[item].groupName}
            </Text>
            <Text style={styles.noOfAnswersOrComments}>
              {userData?.group_outerDetails?.[item].noOfAnswersOrComments}
            </Text>
          </View>
        </View>
        <View>
          <Text style={styles.lastMessageTime}>
            {userData?.group_outerDetails?.[item].lastMessageTime}
          </Text>
          <View style={styles.indicator} />
        </View>
      </TouchableOpacity>
    );
  };

export const ConversationsScreen = ({navigation}) => {
  const {setSortedContacts, setUserData, firebaseUserDetails, userData} =
    useContext(AppContext);
  const [permissionGranted, setPermissionGranted] = useState(null);
  const [loader, setLoader] = useState(false);
  const remainingDays = useRef(7 - new Date().getDay());
  const userContactNumber = firebaseUserDetails?.phoneNumber
    .replace(/\D/g, '')
    .slice(-10);

  useEffect(() => {
    (async () => {
      setLoader(true);
      checkContactPermission(setPermissionGranted, setSortedContacts);
      const response = await fetchDataFromFirebase(
        `users/${userContactNumber}`,
      );
      setUserData(response);
      setLoader(false);
    })();
  }, []);

  const navigateToGroupsScreen = async groupId => {
    setLoader(true);
    console.log(groupId);
    const groupDetails = await fetchDataFromFirebase(`groups/${groupId}`);
    navigation.navigate('collegeBuds', {
      groupDetails,
    });
    setLoader(false);
  };

  const thisweekSparkView = useMemo(() => {
    return (
      <View style={{flex: 1}}>
        <TouchableOpacity
          style={styles.sparkView}
          onPress={() => navigation.navigate('thisWeekSparkHomeScreen')}>
          <Image
            style={styles.sparkImage}
            source={require('../assets/images/leftSpark.png')}
          />
          <Text style={styles.sparkHeaderText}>This week spark</Text>
          <Text style={styles.sparkRemainingTimeText}>
            {remainingDays.current} days left
          </Text>
        </TouchableOpacity>

        <FlatList
          data={Object.keys(userData?.group_outerDetails || {})}
          contentContainerStyle={styles.flatList}
          keyExtractor={(item, index) =>
            userData?.group_outerDetails[item]?.groupName + index
          }
          renderItem={groups(navigateToGroupsScreen, userData)}
        />
      </View>
    );
  }, [userData?.group_outerDetails]);

  return (
    <View style={styles.containerView}>
      <LoadingIndicator visible={loader} />
      <StatusBar
        backgroundColor={'rgb(250, 251, 253)'}
        barStyle="dark-content"
      />
      {!permissionGranted && permissionGranted !== null ? (
        <AskPermissions setPermissionGranted={setPermissionGranted} />
      ) : (
        thisweekSparkView
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  containerView: {
    flex: 1,
    backgroundColor: 'rgb(250,251,253)',
  },
  sparkView: {
    marginHorizontal: 11,
    marginVertical: 14,
    paddingVertical: 12,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgb(255, 255, 255)',
    elevation: 20,
    shadowColor: 'rgba(6, 51, 164, 0.5)',
  },
  sparkImage: {
    position: 'absolute',
    width: 52,
    height: 52,
    left: 0,
    top: 0,
  },
  sparkHeaderText: {
    ...FontUtils.font({
      fontSize: 16,
      color: 'rgb( 3, 6, 71)',
      letterSpacing: 0,
      fontFamily: 'Raleway-Bold',
    }),
  },
  sparkRemainingTimeText: {
    ...FontUtils.font({
      fontSize: 12,
      color: 'rgb( 17,17,17)',
      letterSpacing: 0,
      fontFamily: 'Raleway-Light',
    }),
    marginTop: 2,
  },
  flatList: {
    borderTopColor: 'rgb(217,217,217)',
    borderTopWidth: 1,
  },
  groupOrUserView: {
    paddingHorizontal: 11,
    paddingVertical: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    borderBottomColor: 'rgb(217,217,217)',
    borderBottomWidth: 1,
  },
  imageAndNameView: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  groupOrUserImage: {
    width: 40,
    height: 40,
    borderRadius: 40,
  },
  groupOrUserNameAndStatus: {
    marginLeft: 10,
    paddingTop: 2,
  },
  groupOrUserName: {
    ...FontUtils.font({
      fontSize: 14,
      color: 'rgb(17,17,17)',
      letterSpacings: 0,
      fontFamily: 'Raleway-Bold',
    }),
  },
  noOfAnswersOrComments: {
    ...FontUtils.font({
      fontSize: 14,
      color: 'rgb(255,52,52)',
      letterSpacings: 0,
      fontFamily: 'Raleway-Light',
    }),
    marginTop: 2,
  },
  lastMessageTime: {
    ...FontUtils.font({
      fontSize: 14,
      color: 'rgb(17,17,17)',
      letterSpacings: 0,
    }),
  },
  indicator: {
    marginTop: 5,
    paddingVertical: 5,
    paddingHorizontal: 5,
    backgroundColor: 'rgb(59,59,59)',
    borderRadius: 40,
    alignSelf: 'flex-end',
  },
});
