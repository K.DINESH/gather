import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import React, {useContext, useState} from 'react';
import InputText from '../../components/inputText';
import Button from '../../components/button';
import auth from '@react-native-firebase/auth';
import OtpModal from '../../components/otpModal';
import LoadingIndicator from '../../components/loadingModal';
import {AppContext} from '../../context/useContext';
import {FontUtils, showToast} from '../../utilities/fontutilities';

export const LogInScreen = ({navigation}) => {
  const [userCredentials, setUserCredentials] = useState({
    countryCode: '91',
    nationalNumber: '',
  });
  const [otp, setOtp] = useState();
  const [response, setResponse] = useState(null);
  const [modal, setModal] = useState({loading: false, otp: false});
  const {mobileNumbers} = useContext(AppContext);

  const signInWithPhoneNumber = async phoneNumber => {
    phoneNumber = `+${91}${phoneNumber}`;
    setModal(prv => ({...prv, loading: true}));
    try {
      const confirmation = await auth().signInWithPhoneNumber(phoneNumber);
      setResponse(confirmation);
    } catch (error) {}
    setOtp('');
    setModal(prv => ({...prv, loading: false, otp: true}));
  };

  const confirmCode = async () => {
    setModal(prv => ({...prv, loading: true}));
    try {
      await response.confirm(otp);
      navigation.replace('conversationScreen');
    } catch (error) {
      showToast('Invalid Otp');
    }
    setModal(prv => ({...prv, otp: false, loading: false}));
  };

  const validate = () => {
    if (userCredentials?.nationalNumber?.length != 10) {
      showToast('Please enter valid phone number');
    } else {
      let check = false;
      for (let number in mobileNumbers.current) {
        if (number == userCredentials?.nationalNumber) check = true;
      }
      if (check) signInWithPhoneNumber(userCredentials?.nationalNumber);
      else showToast('Opps no account found, Please register');
    }
  };

  return (
    <View style={styles.containerView}>
      <View style={styles.subView}>
        <InputText
          placeholder={'Phone Number'}
          onChangeText={value =>
            setUserCredentials(prv => ({...prv, nationalNumber: value}))
          }
          value={userCredentials.nationalNumber}
          keyboardType={'phone-pad'}
        />
        <Button
          backgroundColor={'skyblue'}
          onPress={validate}
          textColor={'white'}
          text={'Continue'}
        />
      </View>
      <OtpModal
        visible={modal.otp}
        confirmCode={confirmCode}
        setResponse={setResponse}
        otp={otp}
        setOtp={setOtp}
        setModal={setModal}
      />
      <TouchableOpacity
        style={styles.registrationView}
        onPress={() => navigation.navigate('registrationScreen')}>
        <Text style={styles.registrationText}>Registration</Text>
      </TouchableOpacity>
      <LoadingIndicator visible={modal?.loading} />
    </View>
  );
};

const styles = StyleSheet.create({
  containerView: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  subView: {
    alignSelf: 'stretch',
    margin: 20,
  },
  registrationView: {
    position: 'absolute',
    bottom: 30,
    alignSelf: 'center',
  },
  registrationText: {
    ...FontUtils.font({
      fontSize: 16,
      color: 'skyblue',
      letterSpacing: 0,
      fontFamily: 'Railway-Bold',
    }),
  },
});
