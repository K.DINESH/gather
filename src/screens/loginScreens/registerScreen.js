import {View, StyleSheet} from 'react-native';
import React, {useContext, useState} from 'react';
import InputText from '../../components/inputText';
import Button from '../../components/button';
import auth from '@react-native-firebase/auth';
import OtpModal from '../../components/otpModal';
import LoadingIndicator from '../../components/loadingModal';
import {
  imagePicker,
  setDataToFirebase,
  setImageToFirebase,
  showToast,
} from '../../utilities/fontutilities';
import {AppContext} from '../../context/useContext';

export const RegistrationScreen = ({navigation}) => {
  const [userCredentials, setUserCredentials] = useState({
    userName: '',
    countryCode: '91',
    nationalNumber: '',
    imageUri: '',
  });
  const [otp, setOtp] = useState();
  const [response, setResponse] = useState(null);
  const [modal, setModal] = useState({loading: false, otp: false});
  const {mobileNumbers} = useContext(AppContext);

  const signInWithPhoneNumber = async phoneNumber => {
    setModal(prv => ({...prv, loading: true}));
    phoneNumber = `+${91}${phoneNumber}`;
    try {
      const confirmation = await auth().signInWithPhoneNumber(phoneNumber);
      setResponse(confirmation);
    } catch (error) {}
    setOtp('');
    setModal(prv => ({...prv, loading: false, otp: true}));
  };

  const confirmCode = async () => {
    setModal(prv => ({...prv, loading: true}));
    try {
      const user = await response.confirm(otp);
      mobileNumbers.current[userCredentials.nationalNumber] =
        userCredentials.nationalNumber;
      const phoneDetails = {
        nationalNumber: userCredentials.nationalNumber,
        countryCode: userCredentials.countryCode,
        userId: userCredentials.nationalNumber,
      };

      const uri = await setImageToFirebase({
        type: 'profile_',
        imageName: userCredentials.nationalNumber,
        imageUri: userCredentials?.imageUri,
      });

      if (user?.additionalUserInfo?.isNewUser) {
        const setData = [
          {
            value: {
              phone: phoneDetails,
              name: userCredentials?.userName,
              imageUri: uri,
            },
            ref: 'users',
            key: userCredentials.nationalNumber,
          },
          {
            value: userCredentials.nationalNumber,
            ref: 'mobile_numbers',
            key: userCredentials.nationalNumber,
          },
        ];

        await Promise.all(
          setData.map(item => {
            setDataToFirebase({
              value: item.value,
              ref: item.ref,
              key: item.key,
            });
          }),
        );
        navigation.replace('conversationScreen');
      }
    } catch (error) {
      console.log({error});
      showToast('Invalid Otp');
    }
    setModal(prv => ({...prv, otp: false, loading: false}));
  };

  const validate = () => {
    let check = true;
    console.log(userCredentials);
    for (let key in userCredentials) {
      if (userCredentials?.[key]?.length === 0 && key != 'nationalNumber') {
        check = false;
        if (key === 'imageUri') showToast('Please upload image');
        else showToast('Please enter name');
        break;
      } else if (
        key === 'nationalNumber' &&
        userCredentials?.nationalNumber?.length != 10
      ) {
        check = false;
        showToast('Please enter valid phone number');
        break;
      }
    }
    if (check === true) signInWithPhoneNumber(userCredentials?.nationalNumber);
  };

  const pickImage = async () => {
    const imageUri = await imagePicker();
    setUserCredentials(prv => ({...prv, imageUri: imageUri}));
  };

  return (
    <View style={styles.containerView}>
      <View style={styles.subView}>
        <InputText
          placeholder={'Name'}
          onChangeText={value =>
            setUserCredentials(prv => ({...prv, userName: value}))
          }
          value={userCredentials.userName}
        />
        <InputText
          placeholder={'Phone Number'}
          onChangeText={value =>
            setUserCredentials(prv => ({...prv, nationalNumber: value}))
          }
          value={userCredentials.nationalNumber}
          keyboardType={'phone-pad'}
        />
        <Button
          backgroundColor={'skyblue'}
          onPress={pickImage}
          textColor={'white'}
          text={'Upload image'}
        />
        <Button
          backgroundColor={'skyblue'}
          onPress={validate}
          textColor={'white'}
          text={'Continue'}
        />
      </View>
      <OtpModal
        visible={modal.otp}
        confirmCode={confirmCode}
        setResponse={setResponse}
        otp={otp}
        setOtp={setOtp}
        setModal={setModal}
      />
      <LoadingIndicator visible={modal?.loading} />
    </View>
  );
};

const styles = StyleSheet.create({
  containerView: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  subView: {
    alignSelf: 'stretch',
    margin: 20,
  },
});
