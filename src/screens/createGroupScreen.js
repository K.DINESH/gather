import React, {useContext, useState} from 'react';
import {Image, ScrollView, StyleSheet, View} from 'react-native';
import {CustomButton} from '../components/customButton';
import {InputField} from '../components/inputField';
import LoadingIndicator from '../components/loadingModal';
import {NavigationButton} from '../components/navigationButton';
import {AppContext} from '../context/useContext';
import {FontUtils, validateDetails} from '../utilities/fontutilities';
import {imagePicker} from '../utilities/fontutilities';

export const CreateGroupScreen = ({route, navigation}) => {
  const [groupDetails, setGroupDetails] = useState({
    groupName: null,
    groupDescription: null,
    imageUri: null,
  });
  const [loader, setLoader] = useState(false);
  const {userData} = useContext(AppContext);
  const participants = route.params['selectedData'];

  return (
    <ScrollView style={styles.scrollView}>
      <LoadingIndicator visible={loader} />
      <View style={styles.groupImageView}>
        <Image
          style={styles.groupImage}
          source={
            !groupDetails?.imageUri
              ? require('../assets/images/profile.png')
              : {uri: groupDetails.imageUri}
          }
        />
        <NavigationButton
          styling={styles.addGroupPhotoText}
          onPress={async () => {
            const imageUri = await imagePicker();
            setGroupDetails(prv => ({
              ...prv,
              imageUri: imageUri,
            }));
          }}
          text={'Add group photo'}
        />
      </View>
      <View style={styles.textInputView}>
        <InputField
          placeholder={'Group Name'}
          value={groupDetails.groupName}
          onChangeText={value =>
            setGroupDetails(prv => ({...prv, groupName: value}))
          }
        />
        <View style={{marginTop: 22, width: '100%'}}>
          <InputField
            placeholder={'Group description'}
            value={groupDetails.groupDescription}
            onChangeText={value =>
              setGroupDetails(prv => ({...prv, groupDescription: value}))
            }
            multiline={true}
          />
          <View style={{marginTop: 22}}>
            <CustomButton
              text={'Create group'}
              onPress={async () => {
                (await validateDetails({
                  participants,
                  groupDetails,
                  userData,
                  setLoader,
                })) && navigation.replace('conversationScreen');
              }}
            />
          </View>
        </View>
        <Image
          style={styles.appDefaultImage}
          source={require('../assets/images/groupOrUserDp.png')}
        />
        <Image
          style={styles.gatherTextImage}
          source={require('../assets/images/gatherTextImage.png')}
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: 'white',
  },
  groupImageView: {
    backgroundColor: 'white',
    borderBottomColor: 'rgba(151, 151, 151, 0.1)',
    borderBottomWidth: 1,
    alignItems: 'center',
  },
  groupImage: {
    width: 80,
    height: 80,
    marginTop: 21,
    borderRadius: 40,
  },
  addGroupPhotoText: {
    marginTop: 20,
    marginBottom: 14,
    ...FontUtils.font({
      fontSize: 12,
      color: 'rgb(74, 144, 226)',
      letterSpacing: 0,
      fontFamily: 'Raleway-Medium',
    }),
  },
  textInputView: {
    paddingTop: 28,
    paddingHorizontal: 26,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  appDefaultImage: {
    width: 30,
    height: 30,
    marginTop: 177,
    backgroundColor: 'pink',
    borderRadius: 40,
  },
  gatherTextImage: {
    marginTop: 13,
    width: 90,
    height: 34,
    tintColor: 'green',
  },
});
