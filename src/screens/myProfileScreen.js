import React from 'react';
import {
  Image,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {FontUtils} from '../utilities/fontutilities';

export const MyProfileScreen = () => {
  return (
    <ScrollView style={StyleSheet.scrollView}>
      <StatusBar
        backgroundColor={'rgb(250, 251, 253)'}
        barStyle="dark-content"
      />
      <View style={styles.navigationBar} />
      <View style={styles.userView}>
        <View style={styles.profileAndNameView}>
          <Image
            style={styles.imageView}
            source={require('../assets/images/profile.png')}
          />
          <Text style={styles.userNameText}>Michael Smith</Text>
        </View>
        <TouchableOpacity style={styles.editButton}>
          <Text
            style={FontUtils.font({
              fontSize: 18,
              color: 'rgb(154, 154, 154)',
            })}>
            Edit
          </Text>
        </TouchableOpacity>
      </View>
      <View style={styles.bottomView}>
        <View style={styles.settingsView}>
          <Text style={{...styles.settingsText, marginTop: 0}}>
            Share Gather with a friend
          </Text>
          <Text style={styles.settingsText}>About Gather</Text>
          <Text style={styles.settingsText}>Privacy Policy</Text>
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
  },
  navigationBar: {
    marginTop: 22,
    paddingVertical: 18,
    backgroundColor: 'red',
  },
  userView: {
    flexDirection: 'row',
    marginTop: 47,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 27,
  },
  profileAndNameView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  imageView: {
    width: 60,
    height: 60,
    borderRadius: 40,
  },
  userNameText: {
    marginLeft: 18,
    ...FontUtils.font({
      fontSize: 18,
      color: 'rgb(6,51,164)',
      letterSpacing: 0,
      fontFamily: 'Raleway-Medium',
    }),
  },
  editButton: {
    marginLeft: 18,
  },
  bottomView: {
    borderTopWidth: 1,
    borderColor: 'rgb(151, 151, 151)',
    marginTop: 38.5,
  },
  settingsView: {
    marginTop: 38.5,
    paddingHorizontal: 27,
  },
  settingsText: {
    marginTop: 22,
    ...FontUtils.font({
      fontSize: 18,
      color: 'rgb(6,51,164)',
      letterSpacing: 0,
      fontFamily: 'Raleway-Medium',
    }),
  },
});
