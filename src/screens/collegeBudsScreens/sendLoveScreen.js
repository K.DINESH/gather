import React, {useCallback, useContext, useState} from 'react';
import {FlatList, Image, StyleSheet, Text, View} from 'react-native';
import {AppContext} from '../../context/useContext';
import {FontUtils} from '../../utilities/fontutilities';

export const SendLoveScreen = () => {
  const {reactions} = useContext(AppContext);
  const [templates, setTemplates] = useState(
    JSON.parse(JSON.stringify(reactions?.current?.send_love)),
  );

  console.log(templates);

  const flatListRenderItem = useCallback(
    ({item}) => {
      return (
        <View style={styles.templateView}>
          <Text style={styles.title}>{templates[item]?.title}</Text>
          <View style={styles.textView}>
            <Text style={styles.text}>{templates[item]?.text}</Text>
            <Image
              style={styles.image}
              source={require('../../assets/images/sendLoveTemplateImage.png')}
            />
          </View>
        </View>
      );
    },
    [reactions.current.send_love],
  );

  return (
    <FlatList
      data={Object.keys(templates)}
      renderItem={flatListRenderItem}
      ItemSeparatorComponent={() => <View style={styles.lineSeparator} />}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  templateView: {
    backgroundColor: 'white',
    paddingVertical: 13,
    paddingHorizontal: 9,
  },
  title: {
    marginLeft: 11,
    ...FontUtils.font({
      color: 'rgb(37, 139, 128)',
      fontSize: 13,
      letterSpacing: 0.81,
      fontFamily: 'Raleway-SemiBold',
    }),
  },
  textView: {
    marginTop: 13,
    backgroundColor: '#f3e9e1',
    borderRadius: 4,
  },
  text: {
    ...FontUtils.font({
      color: 'rgb(0,0,0)',
      fontSize: 16,
      letterSpacing: 1.28,
      lineHeight: 31,
    }),
    paddingVertical: 12,
    paddingHorizontal: 12,
  },
  image: {
    width: 130,
    height: 50,
    marginBottom: 13,
    alignSelf: 'flex-end',
    marginRight: 13,
  },
  lineSeparator: {
    height: 1,
    backgroundColor: 'rgba(151,151,151,0.2)',
  },
});
