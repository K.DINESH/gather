import React, {
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
  TouchableOpacity,
  BackHandler,
} from 'react-native';
import {CommentTemplate} from '../../components/commentTemplate';
import {KeyBoardAccessory} from '../../components/keyBoardAccessory';
import LoadingIndicator from '../../components/loadingModal';
import {ThisWeekSpark} from '../../components/thisWeekSpark';
import {AppContext} from '../../context/useContext';
import {
  fetchDataFromFirebase,
  findPostTime,
  FontUtils,
  handleBackButtonClick,
  setViewHeight,
} from '../../utilities/fontutilities';

const SparkInGroups = ({spark}) => {
  let count = -1;
  const sparkText = spark?.unfilledSpark?.content?.split(' ');
  const textAnswers = spark?.answers?.text;
  return (
    <View style={styles.thisWeekSparkTextView}>
      {sparkText.map((item, index) => {
        if (item === '____' || item === '____.') {
          count += 1;
          return (
            <Text style={styles.answersText} key={index}>
              {textAnswers?.['answer_' + count]?.value}
              {item === '____.' ? '. ' : ' '}
            </Text>
          );
        } else {
          if (item === '📷.') return;
          return (
            <Text style={styles.thisWeekSparkText} key={index}>
              {item}{' '}
            </Text>
          );
        }
      })}
    </View>
  );
};

export const GroupScreen = ({route, navigation}) => {
  const {groupDetails, sparkId} = route?.params;
  const currentUserId = userData?.phone?.nationalNumber;
  const pathsOfOptions = {
    quetion: require('../../assets/images/quetion.png'),
    send_love: require('../../assets/images/like.png'),
    comment: require('../../assets/images/comment.png'),
  };
  const {userData, reactions, keyboardDetails} = useContext(AppContext);
  const [participants, setParticipants] = useState(
    JSON.parse(JSON.stringify(groupDetails.participants)),
  );
  const [inputComment, setInputComment] = useState({
    value: '',
    customKeyboardIsclicked: false,
  });
  const [heightOf, setHeight] = useState({
    customKeyboard: null,
    accessoryView: null,
  });
  const [focusedTemplate, setFocusedTemplate] = useState({});

  const flatListRef = useRef();

  const onClickOption = ({option, participant, index}) => {
    const numberOfParticipants = Object.keys(participants).length;
    if (option == 'comment') {
      setFocusedTemplate({});
      setParticipants(prv => {
        prv[participant].iscommentClicked = true;
        return {...prv};
      });
      setInputComment(prv => ({
        ...prv,
        customKeyboardIsclicked: true,
      }));
      {
        numberOfParticipants - 1 != index
          ? flatListRef?.current.scrollToIndex({
              animated: true,
              index: index + 1,
            })
          : setTimeout(
              () =>
                flatListRef?.current.scrollToEnd({
                  animated: true,
                }),
              400,
            );
      }
    } else if (option == 'send_love') navigation.navigate('sendLoveScreen');
  };

  const commentView = useCallback(
    ({participant}) => {
      return <View style={styles.commentView}></View>;
    },
    [participants],
  );

  const options = useCallback(
    ({participant, flatListIndex}) => {
      return Object.keys(pathsOfOptions).map((item, index) => {
        return (
          <TouchableOpacity
            onPress={() =>
              onClickOption({
                option: item,
                participant: participant,
                index: flatListIndex,
              })
            }
            key={index}>
            <Image source={pathsOfOptions[item]} style={styles.optionsImages} />
          </TouchableOpacity>
        );
      });
    },
    [participants],
  );

  const flateListRender = useCallback(
    ({item, index}) => {
      const spark = participants?.[item]?.sparks?.[sparkId];
      const sparkImageUri = spark?.answers?.image?.answer_0?.value;
      const userImage = participants?.[item]?.imageUri;
      const userName = participants?.[item]?.name;
      const postTime = findPostTime(spark?.answered?.at);
      const iscommentClicked = participants?.[item]?.iscommentClicked;
      return (
        spark && (
          <View style={styles.itemInFlatList}>
            <Image style={styles.profileImage} source={{uri: userImage}} />
            <View style={styles.sparkView}>
              <Text style={styles.time}>{postTime}</Text>
              <Text style={styles.name}>
                {currentUserId === item ? 'You' : userName}
              </Text>
              <SparkInGroups spark={spark} />
              <Image
                resizeMode="stretch"
                style={styles.sparkImage}
                source={{uri: sparkImageUri}}
              />
              <View style={styles.optionsView}>
                {options({participant: item, flatListIndex: index})}
                {iscommentClicked && commentView({participant: item})}
              </View>
            </View>
          </View>
        )
      );
    },
    [participants],
  );

  const ItemSeparator = useCallback(() => {
    return <View style={styles.separator} />;
  }, [participants]);

  const customKeyboardView = useMemo(() => {
    return (
      <View
        style={{
          height: 230,
          backgroundColor: 'white',
          marginTop: heightOf.accessoryView || 57,
        }}
        onLayout={
          !heightOf.customKeyboard && setViewHeight(setHeight, 'customKeyboard')
        }>
        <FlatList
          data={Object.keys(reactions?.current?.comments || {})}
          renderItem={({item}) => {
            return (
              <CommentTemplate
                templateDetails={reactions?.current?.comments[item]}
                setFocusedTemplate={setFocusedTemplate}
                focusedTemplate={focusedTemplate}
              />
            );
          }}
          contentContainerStyle={{
            backgroundColor: 'white',
            paddingLeft: 19,
            paddingRight: 29,
            paddingVertical: 20,
          }}
          numColumns={3}
          key={'_'}
          columnWrapperStyle={{
            justifyContent: 'space-between',
          }}
          ItemSeparatorComponent={() => <View style={{paddingVertical: 20}} />}
        />
      </View>
    );
  }, [reactions, focusedTemplate, heightOf]);

  const keyBoardAccessoryView = useMemo(() => {
    return (
      <>
        {(inputComment.customKeyboardIsclicked ||
          keyboardDetails.isVisible) && (
          <KeyBoardAccessory
            heightFromBottom={
              inputComment.customKeyboardIsclicked
                ? heightOf.customKeyboard || 0
                : keyboardDetails.height || 255
            }
            inputComment={inputComment}
            setInputComment={setInputComment}
            setHeight={setHeight}
            focusedTemplate={focusedTemplate}
          />
        )}
        {!inputComment.customKeyboardIsclicked && keyboardDetails.isVisible && (
          <>
            <View style={{height: heightOf.accessoryView || 50}} />
            <View
              style={{
                height: keyboardDetails.height || 255,
                backgroundColor: 'white',
              }}
            />
          </>
        )}
      </>
    );
  }, [inputComment, keyboardDetails, heightOf, focusedTemplate]);

  useEffect(() => {
    const backHandler = BackHandler.addEventListener('hardwareBackPress', () =>
      handleBackButtonClick(setInputComment),
    );
    return () => backHandler.remove();
  }, []);

  return (
    <View style={styles.container}>
      <FlatList
        data={Object.keys(participants)}
        renderItem={flateListRender}
        ItemSeparatorComponent={ItemSeparator}
        ListHeaderComponent={() => <ThisWeekSpark buttonPresent={false} />}
        ListHeaderComponentStyle={{backgroundColor: 'white'}}
        ref={flatListRef}
        style={{backgroundColor: 'white'}}
      />
      {keyBoardAccessoryView}
      {inputComment.customKeyboardIsclicked && customKeyboardView}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'rgb(250, 251, 253)',
  },
  itemInFlatList: {
    backgroundColor: 'white',
    flexDirection: 'row',
    paddingHorizontal: 11,
  },
  profileImage: {
    width: 40,
    height: 40,
    borderRadius: 40,
  },
  sparkView: {
    paddingLeft: 14,
    width: '85%',
  },
  name: {
    color: 'rgb(66, 69, 83)',
    fontSize: 17,
  },
  time: {
    position: 'absolute',
    right: 0,
    top: 0,
  },
  thisWeekSparkTextView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 15,
  },
  thisWeekSparkText: {
    ...FontUtils.font({
      fontSize: 15,
      letterSpacing: 0,
      color: 'rgb(66,69,83)',
    }),
    lineHeight: 30,
  },
  answersText: {
    ...FontUtils.font({
      color: 'rgb(0,0,0)',
      fontSize: 15,
      fontFamily: 'Raleway-Bold',
      letterSpacing: 0,
    }),
    lineHeight: 30,
  },
  sparkImage: {width: '100%', height: 224},
  separator: {
    paddingVertical: 10,
  },
  optionsView: {
    flexDirection: 'row',
    paddingVertical: 14,
  },
  optionsImages: {
    width: 32,
    height: 32,
    marginRight: 24,
  },
  commentView: {
    height: 100,
  },
  keyBoardContainer: {
    backgroundColor: 'white',
  },
  keyBoardAccessoryView: {
    flexDirection: 'row',
    position: 'absolute',
    alignItems: 'center',
    paddingVertical: 7,
    height: 50,
    width: '100%',
    backgroundColor: 'white',
    borderTopWidth: 1,
    borderColor: 'rgb(230, 230, 230)',
  },
  textInput: {
    flex: 1,
    borderRadius: 8,
    paddingVertical: 6,
    paddingHorizontal: 11,
    borderWidth: 1,
    borderColor: 'rgb(206, 206, 206)',
    backgroundColor: 'rgb(248, 248, 248)',
    fontSize: 14,
  },
  keyboardImage: {
    width: 35,
    height: 35,
    opacity: 0.36,
    borderRadius: 2,
    marginLeft: 10,
    marginRight: 12,
  },
  postText: {
    ...FontUtils.font({
      color: 'rgb(74 ,144, 226)',
      fontSize: 16,
      letterSpacing: 0,
      fontFamily: 'Raleway-Medium',
    }),
    marginLeft: 11,
    marginRight: 13,
  },
});
