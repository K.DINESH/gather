import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  Modal,
  Image,
  Dimensions,
} from 'react-native';
import React, {useContext, useEffect, useMemo, useRef, useState} from 'react';
import {
  isLandscapeMode,
  FontUtils,
  imagePicker,
  validateAnswers,
  structureAnswers,
} from '../utilities/fontutilities';
import {TextField} from '../components/textField';
import {ImageField} from '../components/imageField';
import {AppContext} from '../context/useContext';
import {CaptionScreen} from './captionScreen';
import {
  Menu,
  MenuOption,
  MenuOptions,
  MenuTrigger,
  MenuProvider,
} from 'react-native-popup-menu';
import {useKeyboardHeight} from '../hooks/useKeyboardHeight';

export function ThisWeekSparkEditingScreen({navigation}) {
  const {keyboardDetails, setKeyBoard, thisWeekSpark} = useContext(AppContext);
  const sparkText = useRef(thisWeekSpark?.current?.text?.content?.split(' '));
  const [answers, setAnswers] = useState(structureAnswers(sparkText.current));
  const [focusedField, setFocusedField] = useState('answer_0');
  const [allFieldsFilled, setAllFieldsFilled] = useState(false);
  const [imageIsFocused, setImageIsFocused] = useState(false);
  const [clearPreviousInterval, setClearPreviousInterval] = useState('');
  const [ismodalVisible, setModalVisible] = useState(false);
  const [isLandscape, setIsLandscape] = useState(false);
  const keyboardInputRef = useRef();
  const dummyTextInputRef = useRef();
  const isImageUploaded = answers?.image?.answer_0?.value?.length != 0;
  let count = -1;

  const keyboardHeight = useKeyboardHeight();

  useEffect(() => {
    navigation.setParams({allFieldsFilled: allFieldsFilled, answers: answers});
  }, [allFieldsFilled]);

  useEffect(() => {
    setAllFieldsFilled(validateAnswers({...answers}));
  }, [answers]);

  useEffect(() => {
    setKeyBoard(prv => ({...prv, isVisible: true}));
    setTimeout(() => keyboardInputRef?.current?.focus(), 1);
    Dimensions.addEventListener('change', ({screen: {width, height}}) => {
      if (width < height) {
        setIsLandscape(false);
      } else {
        setIsLandscape(true);
      }
    });
    return () => clearInterval(clearPreviousInterval);
  }, []);

  if (!imageIsFocused && focusedField === 'image') {
    setTimeout(() => setImageIsFocused(true), 100);
  } else if (imageIsFocused && focusedField !== 'image') {
    setImageIsFocused(false);
  }

  const pickImage = async () => {
    const imageUri = await imagePicker();
    setAnswers(prv => {
      prv.image.answer_0.value = imageUri;
      return {...prv};
    });
    if (imageUri?.length != 0) {
      setModalVisible(true);
    }
  };

  const removeImage = () => {
    setAnswers(prv => {
      let temp = {...prv};
      temp.image.answer_0.value = '';
      return {...temp};
    });
  };

  const nextButtonOnPress = () => {
    if (imageIsFocused) {
      pickImage();
    } else {
      setFocusedField(prv => {
        if (prv == 'answer_' + (Object.keys(answers.text).length - 1)) {
          return 'image';
        }
        const presentField = parseInt(prv[prv.length - 1]) + 1;
        prv = prv.slice(0, -1);
        prv = prv + presentField;
        return prv;
      });
    }
  };

  const accessoryView = useMemo(
    () => (
      <View
        style={{
          ...styles.textInputView,
          bottom: keyboardHeight,
        }}>
        <TextInput
          placeholder={imageIsFocused ? 'Upload Photo' : ''}
          editable={!imageIsFocused}
          style={styles.textInput}
          ref={keyboardInputRef}
          value={answers.text[focusedField]?.value}
          onChangeText={value => {
            setAnswers(prv => {
              let temp = {...prv};
              if (imageIsFocused) return prv;
              temp.text[focusedField].value = value;
              return temp;
            });
          }}
        />
        <TouchableOpacity
          style={styles.nextButtonView}
          onPress={() => nextButtonOnPress()}>
          <Text style={styles.nextText}>NEXT</Text>
        </TouchableOpacity>
      </View>
    ),
    [imageIsFocused, answers, focusedField, keyboardHeight],
  );

  const thisWeekSparkRender = (item, index) => {
    if (item === '____' || item === '____.') {
      // % super key for text field
      count += 1;
      const key = 'answer_' + count;
      return (
        <TextField
          key={index} // unique key to each item
          props={{
            count: count,
            setFocusedField: setFocusedField,
            showCursor: focusedField == key ? true : false,
            text: answers.text?.['answer_' + count].value,
            keyboardControleRef:
              keyboardDetails?.isVisible || isLandscape
                ? keyboardInputRef
                : dummyTextInputRef,
          }}
          intervels={{clearPreviousInterval, setClearPreviousInterval}}
        />
      );
    } else if (item == '📷.') {
      return;
    }
    return (
      <Text style={styles.thisWeekSparkText} key={index}>
        {item}{' '}
      </Text>
    );
  };

  const showMenuOptions = useMemo(() => {
    return (
      isImageUploaded && (
        <MenuProvider>
          <Menu>
            <MenuTrigger>
              <Image
                source={{
                  uri: answers?.image?.answer_0?.value,
                }}
                resizeMode="contain"
                style={styles.image}
              />
            </MenuTrigger>

            <MenuOptions
              customStyles={{
                optionWrapper: {padding: 10},
                optionsContainer: {
                  marginTop: 10,
                  marginLeft: 150,
                  borderRadius: 10,
                },
              }}>
              <MenuOption onSelect={pickImage} text="Change Photo" />
              <MenuOption onSelect={removeImage} text="Remove Photo" />
            </MenuOptions>
          </Menu>
        </MenuProvider>
      )
    );
  }, [isImageUploaded, answers]);

  return (
    <View style={styles.container}>
      <Modal visible={ismodalVisible}>
        <CaptionScreen
          uri={isImageUploaded && answers.image.answer_0.value}
          setModalVisible={setModalVisible}
        />
      </Modal>

      <View style={styles.thisWeekSparkTextView}>
        {sparkText.current?.map(thisWeekSparkRender)}
        {!isImageUploaded && (
          <ImageField
            focused={focusedField === 'image' ? true : false}
            setFocusedField={setFocusedField}
            keyboardControleRef={dummyTextInputRef}
          />
        )}
      </View>

      {showMenuOptions}

      <TextInput
        style={styles.dummyTextInput}
        ref={dummyTextInputRef}
        value={''}
      />
      {(keyboardDetails?.isVisible || isLandscape) && accessoryView}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  thisWeekSparkTextView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    backgroundColor: 'white',
    marginTop: 18,
    paddingHorizontal: 21,
    alignItems: 'center',
  },
  thisWeekSparkText: {
    ...FontUtils.font({
      fontSize: 17,
      letterSpacing: 0,
      color: 'rgb(66,69,83)',
    }),
    lineHeight: 36,
  },
  textInputView: {
    paddingHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    position: 'absolute',
    borderTopWidth: 1,
    borderColor: 'rgb(233, 238, 247)',
    width: '100%',
    backgroundColor: 'white',
  },
  textInput: {
    ...FontUtils.font({
      fontSize: 18,
      color: 'rgb(66, 69, 83)',
      letterSpacing: 0,
      fontFamily: 'Raleway-SemiBold',
    }),
    flex: 1,
  },
  nextButtonView: {
    paddingVertical: 8,
    paddingHorizontal: 8,
    backgroundColor: 'white',
    borderColor: 'rgb(10, 158, 136)',
    borderWidth: 2,
    borderRadius: 4,
  },
  nextText: {
    ...FontUtils.font({
      fontSize: 12,
      letterSpacing: 0.75,
      fontFamily: 'Raleway-Bold',
      color: 'rgb(10, 158, 136)',
    }),
  },
  dummyTextInput: {
    paddingVertical: 0,
    height: 1,
    width: 1,
    backgroundColor: 'white',
    position: 'absolute',
  },
  // selectedImageView: {
  //   alignItems: 'center',
  //   justifyContent: 'flex-start',
  // },
  image: {
    height: '90%',
    width: '100%',
  },
});
