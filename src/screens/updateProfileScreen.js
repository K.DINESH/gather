import React from 'react';
import {
  Image,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  View,
} from 'react-native';
import {FontUtils} from '../utilities/fontutilities';
import {ThickBorderedButton} from '../components/thickBorderedButton';

export const UpdateBasicInfoScreen = () => {
  return (
    <ScrollView
      style={styles.scrollView}
      contentContainerStyle={styles.scrollViewContentContainerStyle}>
      <StatusBar backgroundColor={'rgb(6, 51, 164)'} />
      <View style={styles.container}>
        <Text style={styles.lastStepText}>Last step!</Text>
        <View style={styles.formContainer}>
          <Text style={styles.completeProfileText}>Complete your profile</Text>

          <TextInput
            style={styles.textField}
            placeholder="First name"
            placeholderTextColor={'white'}
          />
          <TextInput
            style={styles.textField}
            placeholder="Last name"
            placeholderTextColor={'white'}
          />
          <View
            style={{
              ...styles.profileImage,
              marginTop: 30,
              elevation: 20,
              shadowColor: 'rgba(0,0,0,1)',
            }}>
            <Image
              source={require('../assets/images/conversationProfile.png')}
              style={styles.profileImage}
            />
          </View>
          <ThickBorderedButton
            text={'Edit photo'}
            buttonStyle={styles.thickBorderedButton}
          />
          <ThickBorderedButton
            text={'Pull from Facebook'}
            buttonStyle={styles.thickBorderedButton}
          />
        </View>
        <ThickBorderedButton
          text={"Let's get started!"}
          buttonStyle={styles.getStartedButton}
          textStyle={styles.getStartedText}
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: 'rgb(6, 51, 164)',
  },
  scrollViewContentContainerStyle: {
    paddingHorizontal: 40,
  },
  container: {
    flex: 1,
  },
  lastStepText: {
    marginTop: 11,
    ...FontUtils.font({
      fontSize: 14,
    }),
    alignSelf: 'center',
  },
  formContainer: {
    marginTop: 39,
    alignItems: 'center',
    paddingHorizontal: 25,
  },
  completeProfileText: {
    ...FontUtils.font({
      fontSize: 22,
    }),
    alignSelf: 'center',
    marginBottom: 9,
  },
  textField: {
    width: '100%',
    borderBottomWidth: 1,
    borderColor: 'white',
    paddingVertical: 0,
    paddingLeft: 6,
    color: 'white',
    marginTop: 28,
    ...FontUtils.font({fontSize: 18}),
  },
  profileImage: {
    height: 80,
    width: 80,
    borderRadius: 40,
  },
  thickBorderedButton: {
    marginTop: 20,
  },
  getStartedButton: {
    marginTop: 111,
    backgroundColor: 'rgb(3, 6, 71)',
    marginBottom: 52,
  },
  getStartedText: {
    ...FontUtils.font({
      fontSize: 20,
      fontFamily: 'Raleway-Bold',
      letterSpacing: 0.28,
    }),
  },
});
