import React from 'react';
import {StyleSheet, View} from 'react-native';
import WebView from 'react-native-webview';

export const SourceCodeWebView = () => {
  return (
    <View style={styles.container}>
      <WebView
        source={{uri: 'https://gitlab.com/K.DINESH/gather'}}
        style={styles.container}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
