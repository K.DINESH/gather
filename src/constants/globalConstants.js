export const PERMISSION_DENIED = 'permission denied';
export const PERMISSION_PROCESSING = 'permission processing';
export const PERMISSION_GRANTED = 'permission granted';
