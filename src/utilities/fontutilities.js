import {
  Dimensions,
  Keyboard,
  PermissionsAndroid,
  ToastAndroid,
} from 'react-native';
import Contacts from 'react-native-contacts';
import auth from '@react-native-firebase/auth';
import DocumentPicker from 'react-native-document-picker';
import database from '@react-native-firebase/database';
import storage from '@react-native-firebase/storage';

export const FontUtils = {
  font: ({
    fontSize,
    lineHeight = fontSize,
    letterSpacing = 0.25,
    color = 'rgb(255, 255, 255)',
    fontFamily = 'Raleway-Regular',
  } = {}) => {
    return {
      fontSize,
      fontFamily,
      lineHeight,
      letterSpacing,
      color,
    };
  },
};

export const keyboardLisner = setKeyBoard => {
  const keyboardDidShowListener = Keyboard.addListener(
    'keyboardDidShow',
    response => {
      setKeyBoard(prv => ({
        ...prv,
        isVisible: true,
        height: response?.endCoordinates?.height,
      }));
    },
  );
  const keyboardDidHideListener = Keyboard.addListener(
    'keyboardDidHide',
    () => {
      setKeyBoard(prv => ({
        ...prv,
        isVisible: false,
      }));
    },
  );

  return () => {
    keyboardDidHideListener.remove();
    keyboardDidShowListener.remove();
  };
};

export const structuringContacts = sortedContacts => {
  let tempList = [];
  let finalContactsData = [];
  let unknowns = [];
  for (let index = 0; index < sortedContacts?.length; index++) {
    const currentNameFirstLatter = sortedContacts[index]?.displayName?.[0]
      ?.trim()
      ?.toUpperCase();
    if (!(currentNameFirstLatter >= 'A' && currentNameFirstLatter <= 'Z')) {
      unknowns.push(sortedContacts[index]);
      continue;
    }
    tempList.push(sortedContacts[index]);
    if (
      currentNameFirstLatter !=
      sortedContacts[index + 1]?.displayName?.[0]?.trim()?.toUpperCase()
    ) {
      finalContactsData.push({title: currentNameFirstLatter, data: tempList});
      tempList = [];
    }
  }
  if (unknowns.length != 0)
    finalContactsData.push({title: '#', data: unknowns});
  return finalContactsData;
};

const fetchContacts = setSortedContacts => {
  Contacts.getAll().then(contacts => {
    let sortedContacts = contacts.sort((item1, item2) => {
      return (
        item1?.displayName?.trim()?.toUpperCase() >
        item2?.displayName?.trim()?.toUpperCase()
      );
    });
    setSortedContacts([...sortedContacts]);
  });
};

export const contactPermissions = (setPermissionGranted, setSortedContacts) => {
  PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS).then(
    response => {
      if (response === 'granted') {
        checkContactPermission(setPermissionGranted, setSortedContacts);
      }
    },
  );
};

export const searchByText = (text, sortedContacts) => {
  const upperCaseText = text.toUpperCase();
  const searchedContacts = sortedContacts.filter(contactData => {
    if (contactData?.displayName?.toUpperCase()?.includes(upperCaseText)) {
      return contactData;
    }
  });

  return structuringContacts(searchedContacts);
};

export const checkContactPermission = (
  setPermissionGranted,
  setSortedContacts,
) => {
  PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.READ_CONTACTS).then(
    response => {
      if (response) {
        fetchContacts(setSortedContacts);
      }
      setPermissionGranted(response);
    },
  );
};

export const imagePicker = async () => {
  const response = await DocumentPicker.pickSingle({
    presentationStyle: 'fullScreen',
    type: [DocumentPicker.types.images],
    copyTo: 'cachesDirectory',
  }).catch(error => console.log(error));
  return response?.fileCopyUri;
};

export const showToast = responseData => {
  ToastAndroid.show(responseData, ToastAndroid.SHORT);
};

export const validateAnswers = answers => {
  for (let key in answers.text) {
    if (answers?.text?.[key]?.value?.length === 0) return false;
  }
  if (answers?.image?.answer_0?.value?.length === 0) return false;
  return true;
};

export const selectAll = (setGroup_outerDetails, setSelectedGroupIds) => {
  let tempList = [];
  setGroup_outerDetails(prv => {
    let tempObject = {};
    for (let key in prv) {
      tempList.push(key);
      tempObject[key] = {...prv[key], isclicked: true};
    }
    prv = {...tempObject};
    return {...prv};
  });
  setSelectedGroupIds(tempList);
};

export const isLandscapeMode = () => {
  const screenHeight = Dimensions.get('screen').height;
  const screenWidth = Dimensions.get('screen').width;
  return screenHeight < screenWidth;
};

export const isKeyPresent = async (ref, searchKey) => {
  let ispresent = false;
  await database.ref(ref).once('value', snapshot => {
    for (key in snapshot.val()) {
      if (key == searchKey) ispresent = true;
    }
  });
  return ispresent;
};

export const fetchDataFromFirebase = async ref => {
  const snapshot = await database().ref(ref).once('value');
  return snapshot.val();
};

export const setDataToFirebase = async ({value, ref, key}) => {
  await database()
    .ref(ref)
    .child(key)
    .set(value)
    .catch(e => console.log(e));
};

export const pushDataToFirebase = async ({value, ref}) => {
  const pushRef = await database()
    .ref(ref)
    .push(value)
    .catch(e => console.log(e));
  return pushRef.key;
};

export const setImageToFirebase = async ({type, imageName, imageUri}) => {
  const imageRef = storage().ref(type + imageName);
  await imageRef.putFile(imageUri).catch(e => console.log(e));
  const uri = await imageRef.getDownloadURL().catch(e => console.log(e));
  return uri;
};

export const passDataToFirebase = async ({
  participants,
  groupDetails,
  userData,
}) => {
  const date = Date.now();
  const firebaseImageUri = await setImageToFirebase({
    type: 'group_image_',
    imageName: groupDetails.groupName,
    imageUri: groupDetails.imageUri,
  });
  let fetchedParticipants = {};

  const users = await Promise.all(
    participants.map(item => {
      return fetchDataFromFirebase(`users/${item}`);
    }),
  );
  users.map(item => {
    fetchedParticipants[item.phone.nationalNumber] = item;
  });
  const group = {
    ...groupDetails,
    indicator: false,
    noOfAnswersOrComments: '',
    lastMessageTime: 'new',
    participants: fetchedParticipants,
    imageUri: firebaseImageUri,
    created: {by: userData?.phone?.nationalNumber, at: date},
  };

  const groupId = await pushDataToFirebase({
    value: group,
    ref: 'groups',
  });
  await setDataToFirebase({
    value: groupId,
    ref: `groups/${groupId}`,
    key: 'group_id',
  });
  const groupOuterDetails = {
    indicator: false,
    noOfAnswersOrComments: '',
    lastMessageTime: 'new',
    imageUri: firebaseImageUri,
    groupName: groupDetails.groupName,
    group_id: groupId,
  };
  await Promise.all(
    participants.map(item => {
      setDataToFirebase({
        value: groupOuterDetails,
        ref: `users/${item}/group_outerDetails`,
        key: groupId,
      });
    }),
  );
  showToast('Group Created');
};

export const validateDetails = async ({
  participants,
  groupDetails,
  userData,
  setLoader,
}) => {
  let allDetailsFilled = true;
  for (let key in groupDetails) {
    if (!groupDetails?.imageUri) {
      showToast('Please upload Image');
      allDetailsFilled = false;
      break;
    }
    if (!groupDetails?.[key]) {
      showToast('Please fill group details');
      allDetailsFilled = false;
      break;
    }
  }
  if (allDetailsFilled) {
    setLoader(true);
    await passDataToFirebase({
      participants,
      groupDetails,
      userData,
    });
    setLoader(false);
    return true;
  }
};

export const shareSparkToGroups = async ({
  filledSpark,
  selectedGroupIds,
  navigation,
  thisWeekSpark,
  userData,
  setLoader,
}) => {
  setLoader(true);
  const date = new Date();
  const imageData = filledSpark?.answers?.image?.answer_0;
  let sparkDetails = {...filledSpark};
  const uri = await setImageToFirebase({
    type: 'spark_Image_',
    imageName: date.getTime(),
    imageUri: imageData.value,
  });
  sparkDetails.answers.image.answer_0.value = uri;

  await Promise.all(
    selectedGroupIds.map(groupId => {
      const ref = `groups/${groupId}/participants/${userData?.phone?.nationalNumber}/sparks`;
      setDataToFirebase({
        value: sparkDetails,
        ref: ref,
        key: thisWeekSpark?.current?.id,
      });
      setDataToFirebase({
        value: sparkDetails?.unfilledSpark,
        ref: `groups/${groupId}/weeks`,
        key: thisWeekSpark?.current?.id,
      });
    }),
  );
  setLoader(false);
  navigation.replace('conversationScreen');
};

export const findPostTime = time => {
  const presentTime = Date.now();
  const remainingTime = presentTime - time;
  let mints = Math.floor(remainingTime / (1000 * 60));
  let hours = Math.floor(remainingTime / (1000 * 60 * 60));
  let days = Math.floor(remainingTime / (1000 * 60 * 60 * 24));
  if (mints == 0) return 'just now';
  if (mints <= 60) return mints + ' min';
  if (hours < 24 && hours != 0) return hours + ' h';
  return days + ' d';
};

export const findWeekNumber = () => {
  const currentDate = new Date();
  const startDate = new Date(currentDate.getFullYear(), 0, 1);
  var days = Math.floor((currentDate - startDate) / (24 * 60 * 60 * 1000));
  const weekNumber = Math.ceil(days / 7);
  return weekNumber < 10 ? '0' + weekNumber : weekNumber;
};

export const structureAnswers = sparkText => {
  let result = {text: {}, image: {}};
  let count = 0;
  sparkText.map(item => {
    if (item === '____' || item === '____.') {
      result.text['answer_' + count] = {
        type: 'text',
        value: '',
      };
      count += 1;
    }
  });
  result.image.answer_0 = {
    imageCaption: '',
    type: 'image',
    value: '',
  };
  return result;
};

export const keyboardHandler = ({
  setKeyBoard,
  setInputComment,
  textInputRef,
  keyboardDetails,
}) => {
  Keyboard.dismiss();
  if (!keyboardDetails.isVisible) {
    textInputRef.current.focus();
    setInputComment(prv => ({
      ...prv,
      customKeyboardIsclicked: false,
    }));
    setKeyBoard(prv => ({...prv, isVisible: true}));
  } else {
    setInputComment(prv => ({
      ...prv,
      customKeyboardIsclicked: true,
    }));
  }
};

export const setViewHeight = (setHeight, viewName) => event => {
  event.persist();
  setHeight(prv => {
    if (prv[viewName] == event?.nativeEvent?.layout?.height) return prv;
    return {...prv, [viewName]: event?.nativeEvent?.layout?.height};
  });
};

export const handleBackButtonClick = setInputComment => {
  let customKeyboardIsVisible;
  setInputComment(prv => {
    if (prv.customKeyboardIsclicked) {
      customKeyboardIsVisible = true;
      return {...prv, customKeyboardIsclicked: false};
    } else {
      customKeyboardIsVisible = false;
      return prv;
    }
  });
  return customKeyboardIsVisible;
};
