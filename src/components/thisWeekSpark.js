import {View, Text, StyleSheet, Image, TextComponent} from 'react-native';
import React, {useContext, useRef} from 'react';
import {FontUtils} from '../utilities/fontutilities';
import {CustomButton} from './customButton';
import {TextField} from './textField';
import {ImageField} from './imageField';
import {AppContext} from '../context/useContext';
import {SparkMapping} from './sparkMap';

export function ThisWeekSpark({navigation, buttonPresent = true}) {
  const {thisWeekSpark} = useContext(AppContext);
  const sparkText = useRef(thisWeekSpark?.current?.text?.content?.split(' '));

  return (
    <View style={styles.thisWeekSparkView}>
      <Image
        source={require('../assets/images/topRightSpark.png')}
        style={styles.topRightSpark}
      />
      <Text style={styles.thisWeekSparkHeaderText}>This week's spark</Text>
      <View style={styles.thisWeekSparkTextView}>
        <SparkMapping
          spark={sparkText?.current}
          imageComponent={<ImageField activate={false} />}
          textFieldComponent={<TextField activate={false} />}
        />
      </View>
      <View style={styles.joinConversation}>
        {buttonPresent && (
          <CustomButton
            text={'Join Conversation'}
            onPress={() => navigation.navigate('thisWeekSparkEditingScreen')}
          />
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: 'white',
  },
  thisWeekSparkView: {
    backgroundColor: 'white',
    marginVertical: 16,
    marginHorizontal: 10,
    paddingHorizontal: 13,
    shadowColor: 'rgba(52, 44, 44, 0.6)',
    elevation: 10,
    borderRadius: 4,
  },
  topRightSpark: {
    position: 'absolute',
    right: -1,
    top: -1,
    width: 34,
    height: 34.7,
  },
  thisWeekSparkTextView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 15,
  },
  thisWeekSparkHeaderText: {
    ...FontUtils.font({
      fontSize: 17,
      letterSpacing: 0,
      color: 'rgb(20, 31, 77)',
      fontFamily: 'Raleway-SemiBold',
    }),
    marginTop: 20,
  },
  thisWeekSparkText: {
    ...FontUtils.font({
      fontSize: 17,
      letterSpacing: 0,
      color: 'rgb(66,69,83)',
    }),
    lineHeight: 36,
  },
  joinConversation: {
    marginTop: 15,
    marginHorizontal: 15,
    marginBottom: 25,
  },
});
