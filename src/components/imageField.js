import React, {useContext} from 'react';
import {Image, Keyboard, StyleSheet, TouchableOpacity} from 'react-native';
import {AppContext} from '../context/useContext';

export const ImageField = ({
  focused = false,
  setFocusedField = () => {},
  keyboardControleRef = {},
  activate = true,
}) => {
  const {keyboardDetails} = useContext(AppContext);
  if (focused) keyboardControleRef?.current?.focus();

  return (
    <TouchableOpacity
      disabled={!activate}
      style={[focused && styles.focusStyle]}
      onPress={() => {
        setFocusedField('image');
        if (!keyboardDetails?.isVisible) {
          Keyboard.dismiss();
          keyboardControleRef?.current?.focus();
        }
      }}>
      <Image
        style={styles.addPhotoImage}
        source={require('../assets/images/addPhoto.png')}
      />
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  addPhotoImage: {
    width: 98,
    height: 24,
  },
  focusStyle: {
    width: 98,
    height: 24,
    borderWidth: 1,
    borderColor: 'rgba( 213, 179, 175, 0.7)',
    shadowColor: 'rgba(213, 179, 175, 0.7)',
    elevation: 10,
    borderRadius: 8,
  },
});
