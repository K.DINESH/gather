import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {FontUtils} from '../utilities/fontutilities';
export const ThickBorderedButton = ({
  text,
  onPress,
  buttonStyle = {},
  textStyle = {},
}) => {
  return (
    <TouchableOpacity
      style={{...styles.thickBorderedButtonContainer, ...buttonStyle}}
      onPress={onPress}>
      <Text style={{...styles.borderedButtonText, ...textStyle}}>{text}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  thickBorderedButtonContainer: {
    paddingVertical: 15,
    borderWidth: 3,
    borderColor: 'white',
    borderRadius: 22.5,
    width: '100%',
    alignItems: 'center',
    backgroundColor: 'rgb(66, 101, 170)',
    elevation: 20,
    shadowColor: `rgba(0, 0, 0, ${Platform.select({ios: 0.5, android: 1})}`,
  },
  borderedButtonText: {
    ...FontUtils.font({fontSize: 18, fontFamily: 'Raleway-Medium'}),
  },
});
