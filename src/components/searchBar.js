import React, {useContext} from 'react';
import {Image, StyleSheet, TextInput, View} from 'react-native';
import {AppContext} from '../context/useContext';
import {searchByText} from '../utilities/fontutilities';

export const SearchBar = ({setSearchedContacts}) => {
  const {sortedContacts} = useContext(AppContext);
  return (
    <View style={styles.container}>
      <Image
        source={require('../assets/images/search.png')}
        style={styles.searchImage}
      />
      <TextInput
        placeholder="Search for a Friend"
        placeholderTextColor={'rgb(140, 140, 140)'}
        style={styles.TextInput}
        onChangeText={value =>
          setSearchedContacts(searchByText(value, sortedContacts))
        }
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: 'white',
    paddingVertical: 6,
    borderRadius: 5,
    alignItems: 'center',
  },
  searchImage: {
    width: 13,
    height: 13,
    marginLeft: 8,
  },
  TextInput: {
    paddingVertical: 0,
    marginLeft: 8,
    width: '100%',
  },
});
