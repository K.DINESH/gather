import React from 'react';
import {StyleSheet, TextInput} from 'react-native';
import {FontUtils} from '../utilities/fontutilities';

export const InputField = ({
  placeholder,
  value,
  onChangeText,
  multiline = false,
}) => {
  return (
    <TextInput
      placeholder={placeholder}
      value={value}
      onChangeText={onChangeText}
      style={styles.inputFieldStyle}
      multiline={multiline}
    />
  );
};

const styles = StyleSheet.create({
  inputFieldStyle: {
    paddingVertical: 14,
    paddingHorizontal: 14,
    backgroundColor: 'rgb(239, 241, 244)',
    ...FontUtils.font({
      fontSize: 16,
      color: 'rgb(0,0,0)',
      letterSpacing: 0,
      fontFamily: 'Raleway-medium',
    }),
    width: '100%',
    borderRadius: 10,
  },
});
