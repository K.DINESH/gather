import {View, Text, Modal, TouchableOpacity} from 'react-native';
import React from 'react';
import InputText from './inputText';

export default function OtpModal({
  visible,
  confirmCode,
  setResponse,
  otp,
  setOtp,
  setModal,
}) {
  return (
    <Modal visible={visible} transparent={true}>
      <View
        style={{
          backgroundColor: '#000000aa',
          flex: 1,
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View
          style={{
            backgroundColor: 'white',
            borderRadius: 10,
            alignSelf: 'stretch',
            marginLeft: 50,
            marginRight: 50,
            borderColor: 'black',
          }}>
          <View
            style={{
              margin: 20,
              marginLeft: 40,
              marginRight: 40,
              alignSelf: 'stretch',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 20, color: 'black'}}>Please Enter OTP</Text>
            <InputText
              placeholder={'OTP'}
              onChangeText={setOtp}
              error={true}
              value={otp}
              keyboardType={'phone-pad'}
            />
            <View
              style={{
                flexDirection: 'row',
                alignSelf: 'stretch',
                justifyContent: 'space-around',
                marginTop: 10,
              }}>
              <TouchableOpacity
                onPress={() => setModal(prv => ({...prv, otp: false}))}>
                <Text style={{fontSize: 20, color: 'black'}}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity
                disabled={otp?.length > 0 && otp?.length < 20 ? false : true}
                onPress={confirmCode}>
                <Text style={{fontSize: 20, color: 'black'}}>Confirm</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </Modal>
  );
}
