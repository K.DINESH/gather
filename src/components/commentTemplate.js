import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {FontUtils} from '../utilities/fontutilities';

export const CommentTemplate = ({
  templateDetails,
  setFocusedTemplate,
  focusedTemplate,
}) => {
  const isFocused = templateDetails?.templateId == focusedTemplate?.templateId;
  return (
    <TouchableOpacity
      style={{
        ...styles.templateContainer,
        borderColor: isFocused ? 'rgb(3, 6, 71)' : 'rgb(151,151,151)',
      }}
      onPress={() => setFocusedTemplate(templateDetails)}>
      <Text
        style={{
          ...FontUtils.font({
            fontSize: 12,
            color: isFocused ? 'rgb(3, 6, 71)' : 'rgb(156,156,156)',
            letterSpacing: 0,
          }),
        }}>
        {templateDetails?.text}
      </Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  templateContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 5,
    paddingLeft: 2,
    paddingRight: 5,
    backgroundColor: 'white',
    borderRadius: 8,
    borderWidth: 1,
    width: '25%',
  },
});
