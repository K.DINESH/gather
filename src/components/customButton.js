import React from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {FontUtils} from '../utilities/fontutilities';

export const CustomButton = ({text, onPress}) => {
  return (
    <TouchableOpacity style={styles.buttonView} onPress={onPress}>
      <Text style={styles.buttonText}>{text}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  buttonView: {
    paddingVertical: 16,
    alignItems: 'center',
    alignSelf: 'stretch',
    backgroundColor: 'rgb(10, 158, 136)',
    borderRadius: 5,
    shadowColor: 'rgba(20, 31, 77, 1)',
    elevation: 10,
  },
  buttonText: {
    ...FontUtils.font({
      fontSize: 15,
      color: 'white',
      letterSpacing: 1.07,
      fontFamily: 'Raleway-SemiBold',
    }),
  },
});
