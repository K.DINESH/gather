import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';

export const Indicator = ({
  showIndicator = false,
  marginRight = 8,
  screenDetails,
  navigation,
}) => {
  return (
    <TouchableOpacity
      style={{
        ...styles.container,
        backgroundColor: showIndicator
          ? 'rgb(66, 69, 83)'
          : 'rgb(216, 216, 216)',
        marginRight: marginRight,
      }}
      onPress={() => navigation.navigate(screenDetails?.screenName)}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    width: 8,
    height: 8,
    borderRadius: 40,
  },
});
