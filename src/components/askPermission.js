import React, {useContext} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {AppContext} from '../context/useContext';
import {contactPermissions, FontUtils} from '../utilities/fontutilities';

export const AskPermissions = ({setPermissionGranted}) => {
  const {setSortedContacts} = useContext(AppContext);
  return (
    <View style={styles.containerView}>
      <View style={styles.textWithCross}>
        <Text style={styles.findFriends}>Find Friends on Gather</Text>
        <Image
          source={require('../assets/images/cross.png')}
          style={styles.crossImage}
        />
      </View>
      <Text style={styles.aboutGather}>
        Gather is about conversations with your friends. We can't help you find
        your friends without access to contacts.
      </Text>
      <TouchableOpacity
        style={styles.buttonView}
        onPress={() =>
          contactPermissions(setPermissionGranted, setSortedContacts)
        }>
        <Text style={styles.button}>Access Contacts</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  containerView: {
    alignItems: 'center',
    backgroundColor: 'rgb(248, 226, 218)',
  },
  textWithCross: {
    marginTop: 12,
    alignSelf: 'stretch',
    alignItems: 'center',
  },
  findFriends: {
    ...FontUtils.font({
      fontSize: 18,
      color: 'rgb(6, 51, 164)',
      letterSpacing: 0,
      fontFamily: 'Raleway-Bold',
    }),
  },
  crossImage: {
    position: 'absolute',
    width: 14,
    height: 14,
    right: 18,
    top: 2,
  },
  aboutGather: {
    ...FontUtils.font({
      fontSize: 12,
      color: 'rgb(6, 51, 164)',
      letterSpacing: 0,
      fontFamily: 'Raleway-Medium',
    }),
    marginTop: 16,
    marginLeft: 20,
    marginRight: 28,
    alignSelf: 'center',
  },
  buttonView: {
    marginTop: 20,
    marginBottom: 13,
    alignItems: 'center',
  },
  button: {
    paddingTop: 11,
    paddingHorizontal: 30,
    paddingBottom: 8,
    borderWidth: 1,
    borderColor: 'rgb(151, 151, 151)',
    borderRadius: 8,
    backgroundColor: 'rgb(2,28,88)',
    ...FontUtils.font({
      fontSize: 12,
      color: 'white',
      letterSpacing: 0,
      fontFamily: 'Raleway-Medium',
    }),
  },
});
