import React, {useContext, useMemo, useRef} from 'react';
import {
  Image,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {AppContext} from '../context/useContext';
import {
  FontUtils,
  keyboardHandler,
  setViewHeight,
} from '../utilities/fontutilities';

export const KeyBoardAccessory = ({
  heightFromBottom,
  inputComment,
  setInputComment,
  setHeight,
  focusedTemplate,
}) => {
  const textInputRef = useRef();
  const {setKeyBoard, keyboardDetails} = useContext(AppContext);

  const tagComment = useMemo(() => {
    return (
      focusedTemplate?.text && (
        <View style={styles.tagCommentView}>
          <Text style={styles.tagCommentText}>{focusedTemplate.text}</Text>
        </View>
      )
    );
  }, [focusedTemplate]);

  return (
    <View
      style={{
        ...styles.keyBoardAccessoryView,
        bottom: heightFromBottom,
      }}
      onLayout={setViewHeight(setHeight, 'accessoryView')}>
      <TouchableOpacity
        onPress={() =>
          keyboardHandler({
            setInputComment,
            textInputRef,
            setKeyBoard,
            keyboardDetails,
          })
        }>
        <Image
          source={require('../assets/images/keyboard.png')}
          style={styles.keyboardImage}
          resizeMode="contain"
        />
      </TouchableOpacity>
      <View style={styles.textInputView}>
        {tagComment}
        <TextInput
          ref={textInputRef}
          style={[
            styles.textInput,
            focusedTemplate?.text && {
              borderTopRightRadius: 0,
              borderTopLeftRadius: 0,
            },
          ]}
          value={inputComment.value}
          onChangeText={value =>
            setInputComment(prv => {
              prv.value = value;
              return {...prv};
            })
          }
          placeholder={
            focusedTemplate?.text ? 'Elaborate or post' : 'Write a comment'
          }
        />
      </View>
      <Text style={styles.postText}>Post</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  keyBoardAccessoryView: {
    flex: 1,
    flexDirection: 'row',
    position: 'absolute',
    alignItems: 'flex-end',
    paddingVertical: 7,
    width: '100%',
    backgroundColor: 'white',
    borderTopWidth: 1,
    borderColor: 'rgb(230, 230, 230)',
  },
  textInput: {
    flex: 1,
    borderRadius: 8,
    paddingVertical: 6,
    paddingHorizontal: 11,
    borderWidth: 1,
    borderColor: 'rgb(206, 206, 206)',
    backgroundColor: 'rgb(248, 248, 248)',
    fontSize: 14,
  },
  keyboardImage: {
    width: 35,
    height: 35,
    opacity: 0.36,
    borderRadius: 2,
    marginLeft: 10,
    marginRight: 12,
    marginBottom: 5,
  },
  postText: {
    ...FontUtils.font({
      color: 'rgb(74 ,144, 226)',
      fontSize: 16,
      letterSpacing: 0,
      fontFamily: 'Raleway-Medium',
    }),
    marginLeft: 11,
    marginRight: 13,
    marginBottom: 11,
  },
  textInputView: {
    flex: 1,
  },
  tagCommentView: {
    paddingTop: 10,
    paddingBottom: 8,
    paddingLeft: 6,
    backgroundColor: 'rgb(248, 248, 248)',
    borderWidth: 1,
    borderBottomWidth: 0,
    borderColor: 'rgb(206, 206, 206)',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  tagCommentText: {
    ...FontUtils.font({
      color: 'rgb(3, 6, 71)',
      fontSize: 14,
      letterSpacing: 0,
    }),
    backgroundColor: 'rgba( 0, 132, 117, 0.1)',
    paddingHorizontal: 8,
    paddingVertical: 3,
    borderRadius: 9,
    alignSelf: 'flex-start',
  },
});
