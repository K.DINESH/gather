import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {FontUtils} from '../utilities/fontutilities';

export const SparkMapping = ({spark, imageComponent, textFieldComponent}) => {
  return spark.map((item, index) => {
    if (item === '📷.') return <View key={index}>{imageComponent}</View>;
    return item === '____' || item === '____.' ? (
      <View key={index}>{textFieldComponent}</View>
    ) : (
      <Text style={styles.thisWeekSparkText} key={index}>
        {item}{' '}
      </Text>
    );
  });
};

const styles = StyleSheet.create({
  thisWeekSparkText: {
    ...FontUtils.font({
      fontSize: 17,
      letterSpacing: 0,
      color: 'rgb(66,69,83)',
    }),
    lineHeight: 36,
  },
});
