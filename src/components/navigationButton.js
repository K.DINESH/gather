import React from 'react';
import {Text, TouchableOpacity} from 'react-native';

export const NavigationButton = ({styling, onPress, text}) => {
  return (
    <TouchableOpacity onPress={onPress}>
      <Text style={styling}>{text}</Text>
    </TouchableOpacity>
  );
};
