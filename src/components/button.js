import {Text, TouchableOpacity, Image} from 'react-native';
import React, {useRef} from 'react';

export default function Button({onPress, backgroundColor, textColor, text}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={{
        alignItems: 'center',
        backgroundColor: backgroundColor,
        borderRadius: 5,
        marginTop: 20,
        paddingVertical: 10,
        paddingHorizontal: 12,
        borderColor: textColor,
        borderWidth: 1,
        alignSelf: 'stretch',
      }}>
      <Text
        style={{
          color: textColor,
          fontSize: 16,
          fontWeight: '500',
        }}>
        {text}
      </Text>
    </TouchableOpacity>
  );
}
