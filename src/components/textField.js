import React, {useContext, useEffect, useState} from 'react';
import {Keyboard, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {AppContext} from '../context/useContext';
import {FontUtils} from '../utilities/fontutilities';

const Cursor = ({blinking}) => {
  const [showText, setShowText] = useState(false);
  useEffect(() => {
    blinking(setShowText);
  }, []);
  return showText ? (
    <Text
      style={{
        ...styles.sparkText,
        color: 'rgb(213, 179, 175)',
      }}>
      |
    </Text>
  ) : (
    <View style={{height: '100%', width: 4}} />
  );
};

export const TextField = ({activate = true, props = {}, intervels = {}}) => {
  const {keyboardDetails} = useContext(AppContext);
  const {clearPreviousInterval, setClearPreviousInterval} = intervels;
  const {text, setFocusedField, count, showCursor, keyboardControleRef} = props;

  const blinking = setShowText => {
    clearInterval(clearPreviousInterval);
    const interval = setInterval(() => {
      setShowText(showText => !showText);
    }, 500);
    setClearPreviousInterval(interval);
  };

  if (showCursor && keyboardDetails?.isVisible)
    setTimeout(() => keyboardControleRef?.current?.focus(), 1);

  return (
    <TouchableOpacity
      style={[styles.sparkView, showCursor && styles.focusStyle]}
      disabled={!activate}
      onPress={() => {
        if (activate) {
          setFocusedField('answer_' + count);
          if (!keyboardDetails?.isVisible) {
            Keyboard.dismiss();
            keyboardControleRef?.current?.focus();
          }
        }
      }}>
      <Text style={styles.sparkText}>{text}</Text>
      {showCursor && <Cursor blinking={blinking} />}
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  sparkText: {
    ...FontUtils.font({
      fontSize: 17,
      color: 'rgb(0,0,0)',
      letterSpacing: 0,
      fontFamily: 'Raleway-SemiBold',
    }),
  },

  sparkView: {
    backgroundColor: 'rgb(249, 250, 252)',
    justifyContent: 'flex-start',
    borderRadius: 2,
    borderWidth: 1,
    borderColor: 'rgba(213,219,230,0.6)',
    height: 26,
    minWidth: 65,
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 5,
    marginBottom: -100,
    marginRight: 5,
  },

  focusStyle: {
    borderColor: 'rgba( 213, 179, 175, 0.7)',
    shadowColor: 'rgba(213, 179, 175, 0.7)',
    elevation: 10,
  },
});
