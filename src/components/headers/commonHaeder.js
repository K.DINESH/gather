import React from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {FontUtils} from '../../utilities/fontutilities';

export const CommonHeader = ({navigation, title}) => {
  return (
    <View style={styles.header}>
      <Text style={styles.centerText}>{title}</Text>
      <TouchableOpacity
        style={styles.cancelView}
        onPress={() => navigation.goBack()}>
        <Image
          style={styles.backArrowImage}
          source={require('../../assets/images/backArrow.png')}
        />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    justifyContent: 'center',
    alignSelf: 'stretch',
    alignItems: 'center',
    paddingVertical: 13,
    backgroundColor: 'rgb(250, 251, 253)',
    borderBottomColor: 'rgba(151, 151, 151, 0.1)',
    borderBottomWidth: 1,
  },
  centerText: {
    ...FontUtils.font({
      fontSize: 16,
      color: 'rgb(36, 44, 55)',
      letterSpacing: 1,
      fontFamily: 'Raleway-Bold',
    }),
  },
  cancelView: {
    position: 'absolute',
    left: 12,
    paddingVertical: 10,
    paddingRight: 20,
  },
  backArrowImage: {width: 8, height: 14, tintColor: 'rgb(66, 69, 83)'},
});
