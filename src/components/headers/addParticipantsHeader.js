import React, {useContext} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {AppContext} from '../../context/useContext';
import {FontUtils} from '../../utilities/fontutilities';
import {NavigationButton} from '../navigationButton';

export const AddParticipantsHeader = ({navigation, selectedData}) => {
  const {userData, mobileNumbers} = useContext(AppContext);

  const gatherMobileNumbers = Object.keys(mobileNumbers.current);

  const navigateToConvertionScreen = () => {
    navigation.navigate('conversationScreen');
  };

  const structuringNumbers = selectedData => {
    const result = [];
    selectedData.map(contacts =>
      contacts?.data?.phoneNumbers.map(numbers => {
        const number = numbers?.number
          ?.split(' ')
          ?.join('')
          ?.replace(/\D/g, '')
          ?.slice(-10);
        if (gatherMobileNumbers.includes(number)) result.push(number);
      }),
    );
    result.push(userData?.phone?.nationalNumber);
    return result;
  };

  const navigateToCreateGroupScreen = () => {
    if (selectedData?.length > 0)
      navigation.replace('createGroupScreen', {
        selectedData: structuringNumbers([...selectedData]),
      });
  };

  return (
    <View style={styles.container}>
      <NavigationButton
        styling={styles.cancelText}
        onPress={navigateToConvertionScreen}
        text={'Cancel'}
      />
      <View style={styles.addParticipantsAndCount}>
        <Text style={styles.addParticipantsText}>Add Participants</Text>
        <Text style={styles.numberOfContacts}>
          {selectedData?.length || 0}/50
        </Text>
      </View>
      <NavigationButton
        styling={{
          ...styles.nextText,
          color:
            selectedData?.length <= 0
              ? 'rgb(144, 144, 144)'
              : 'rgb(74, 144, 226)',
        }}
        onPress={navigateToCreateGroupScreen}
        text={'Next'}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgb(250, 251, 253)',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  cancelText: {
    ...FontUtils.font({
      color: 'rgb(74, 144, 226)',
      fontSize: 16,
      letterSpacing: 0,
    }),
    marginTop: 24,
    marginLeft: 11,
    marginBottom: 19,
  },
  addParticipantsAndCount: {
    marginTop: 20,
    marginBottom: 11,
    alignItems: 'center',
    justifyContent: 'center',
  },
  addParticipantsText: {
    ...FontUtils.font({
      color: 'rgb(17, 17, 17)',
      fontSize: 16,
      letterSpacing: 0,
      fontFamily: 'Raleway-Bold',
    }),
  },
  nextText: {
    ...FontUtils.font({
      color: 'rgb(144, 144, 144)',
      fontSize: 16,
      letterSpacing: 0,
    }),
    marginTop: 24,
    marginRight: 17,
    marginBottom: 19,
  },
});
