import React, {useCallback, useContext} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {AppContext} from '../../context/useContext';
import {FontUtils} from '../../utilities/fontutilities';

export const ConvertionHeader = ({navigation}) => {
  const {userData} = useContext(AppContext);

  const imageButton = useCallback(({style, navigationScreen, imagePath}) => {
    return (
      <TouchableOpacity onPress={() => navigation.navigate(navigationScreen)}>
        <Image style={style} source={imagePath} />
      </TouchableOpacity>
    );
  }, []);

  return (
    <View style={styles.headerView}>
      <Image
        style={styles.headerImageView}
        source={{uri: userData?.imageUri}}
      />
      <View style={styles.centerView}>
        <Text style={styles.headerText}>Conversations</Text>
      </View>
      <View style={{flexDirection: 'row'}}>
        {imageButton({
          style: styles.info,
          navigationScreen: 'sourceCodeWebView',
          imagePath: require('../../assets/images/info.png'),
        })}
        {imageButton({
          style: styles.editButtonImage,
          navigationScreen: 'addParticipantsScreen',
          imagePath: require('../../assets/images/editButton.png'),
        })}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  headerView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingVertical: 14,
    paddingHorizontal: 14,
    backgroundColor: 'rgb(250, 251, 253)',
    borderBottomColor: 'rgb(217,217,217)',
    borderBottomWidth: 1,
  },
  headerImageView: {
    width: 32,
    height: 32,
    borderRadius: 40,
  },
  centerView: {
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    top: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    ...FontUtils.font({
      color: 'rgb(17, 17 ,17)',
      fontSize: 16,
      letterSpacing: 0,
      fontFamily: 'Raleway-Bold',
    }),
  },
  editButtonImage: {
    width: 24,
    height: 24,
    marginLeft: 20,
  },
  info: {
    width: 20,
    height: 20,
    tintColor: 'grey',
  },
});
