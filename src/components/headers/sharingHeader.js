import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {FontUtils} from '../../utilities/fontutilities';

export const SharingHeader = ({navigation}) => {
  return (
    <View style={styles.header}>
      <Text style={styles.centerText}>Share your response</Text>
      <TouchableOpacity
        style={styles.cancelView}
        onPress={() => navigation.goBack()}>
        <Text style={styles.cancel}>Cancel</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  header: {
    justifyContent: 'center',
    paddingVertical: 26,
    backgroundColor: 'rgb(250, 251, 253)',
    alignSelf: 'stretch',
    alignItems: 'center',
    borderBottomColor: 'rgba(151, 151, 151, 0.1)',
    borderBottomWidth: 1,
  },
  centerText: {
    ...FontUtils.font({
      fontSize: 16,
      color: 'rgb(17,17,17)',
      letterSpacing: 0,
      fontFamily: 'Raleway-Bold',
    }),
  },
  cancelView: {
    position: 'absolute',
    right: 14,
  },
  cancel: {
    ...FontUtils.font({
      fontSize: 16,
      color: 'rgb(74, 144, 226)',
      letterSpacing: 0,
    }),
  },
});
