import React, {useContext} from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import {AppContext} from '../../context/useContext';
import {FontUtils} from '../../utilities/fontutilities';

export const ThisWeekSparkEditingHeader = ({
  navigation,
  allFieldsFilled,
  answers,
}) => {
  const {setFilledSpark, thisWeekSpark} = useContext(AppContext);

  return (
    <View style={styles.container}>
      <TouchableOpacity
        onPress={() => navigation.navigate('thisWeekSparkHomeScreen')}>
        <Image
          style={styles.backArrowImage}
          source={require('../../assets/images/backArrow.png')}
        />
      </TouchableOpacity>
      <View style={styles.headerView}>
        <Text style={styles.headerText}>Fill Spark</Text>
      </View>
      <View opacity={allFieldsFilled ? 1 : 0.5}>
        <TouchableOpacity
          style={styles.buttonView}
          disabled={!allFieldsFilled}
          onPress={() => {
            navigation.navigate('sharingScreen'),
              setFilledSpark({
                unfilledSpark: thisWeekSpark.current.text,
                answers: {...answers},
                answered: {
                  at: Date.now(),
                },
              });
          }}>
          <Text style={styles.buttonText}>SHARE</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'rgb(250, 251, 253)',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomColor: 'rgba(151, 151, 151, 0.1)',
    borderBottomWidth: 1,
    paddingVertical: 8,
    paddingHorizontal: 10,
  },
  backArrowImage: {width: 11.1, height: 18.6, tintColor: 'rgb(66, 69, 83)'},
  headerView: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    ...FontUtils.font({
      color: 'rgb(0, 0, 0)',
      fontSize: 17,
      letterSpacing: 0,
      fontFamily: 'Raleway-Medium',
    }),
  },
  buttonView: {
    backgroundColor: 'rgb(10, 158, 136)',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 4,
  },
  buttonText: {
    ...FontUtils.font({
      color: 'rgb(255, 255, 255 )',
      fontSize: 11,
      letterSpacing: 1,
      fontFamily: 'Raleway-ExtraBold',
    }),
  },
});
