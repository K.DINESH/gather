import React, {useContext, useEffect, useRef, useState} from 'react';
import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {AppContext} from '../../context/useContext';
import {FontUtils} from '../../utilities/fontutilities';
import {Indicator} from '../indicator';
import LoadingIndicator from '../loadingModal';

export const CollegeBudsHeader = ({navigation, weeks}) => {
  const indicatorsList = useRef();
  const [loader, setLoader] = useState(false);
  const {collegeBudsRef} = useContext(AppContext);
  const currentScreenIndex = collegeBudsRef?.current?.getState()?.index;

  useEffect(() => {
    setLoader(true);
    let screens = [];
    for (let key in weeks) {
      screens.push({
        screenName: key,
      });
    }
    indicatorsList.current = [...screens];
    setLoader(false);
  }, []);

  return (
    <View style={styles.container}>
      <LoadingIndicator visible={loader} />
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image
          style={styles.backArrowImage}
          source={require('../../assets/images/backArrow.png')}
        />
      </TouchableOpacity>
      <View style={styles.addParticipantsAndCount}>
        <Text style={styles.addParticipantsText}>College Buds</Text>
        <View style={styles.indicatorView}>
          {indicatorsList.current?.map((item, index) => {
            return (
              <Indicator
                key={index}
                showIndicator={currentScreenIndex === index}
                screenDetails={item}
                navigation={collegeBudsRef.current}
              />
            );
          })}
        </View>
      </View>
      <Image
        style={styles.infoIndicator}
        source={require('../../assets/images/info.png')}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'white',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 10,
    borderBottomColor: 'rgb(233, 238, 247)',
    borderBottomWidth: 1,
  },
  addParticipantsAndCount: {
    marginTop: 20,
    marginBottom: 11,
    alignItems: 'center',
    justifyContent: 'center',
    alignItems: 'center',
  },
  addParticipantsText: {
    ...FontUtils.font({
      color: 'rgb(17, 17, 17)',
      fontSize: 16,
      letterSpacing: 0,
      fontFamily: 'Raleway-Bold',
    }),
  },
  //   cancelView: {marginBottom: 19},
  backArrowImage: {width: 11.1, height: 18.6, tintColor: 'rgb(66, 69, 83)'},
  indicatorView: {
    flexDirection: 'row',
    marginTop: 7,
  },
  infoIndicator: {
    width: 18,
    height: 18,
  },
});
