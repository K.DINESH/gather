import {View, Text, TextInput, StyleSheet} from 'react-native';
import React from 'react';

export default function InputText(props) {
  const {
    placeholder,
    onChangeText,
    secureText,
    firstTextInput,
    value,
    editable,
    keyboardType,
    autoCapitalize,
    textAlign,
  } = props;
  return (
    <View>
      <TextInput
        textAlign={textAlign || 'left'}
        keyboardType={keyboardType}
        secureTextEntry={secureText}
        placeholder={placeholder}
        value={value}
        onChangeText={onChangeText}
        editable={editable}
        style={[styles.textInputStyle, {marginTop: firstTextInput ? 0 : 20}]}
        autoCapitalize={autoCapitalize}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  textInputStyle: {
    alignSelf: 'stretch',
    borderWidth: 2,
    borderRadius: 5,
    paddingVertical: 10,
    paddingHorizontal: 12,
    fontSize: 16,
    borderColor: 'grey',
  },
});
