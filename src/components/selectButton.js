import React from 'react';
import {StyleSheet, View} from 'react-native';

export const SelectButton = ({select}) => {
  return (
    <View
      style={{
        ...styles.button,
        backgroundColor: select ? 'rgb(6, 51, 164)' : 'white',
      }}
    />
  );
};

const styles = StyleSheet.create({
  button: {
    width: 25,
    height: 25,
    borderWidth: 1,
    borderColor: 'rgb(151, 151, 151)',
    borderRadius: 40,
  },
});
