import {useEffect, useState} from 'react';
import {Keyboard} from 'react-native';

export const useKeyboardHeight = (initialValue = 0) => {
  const [keyboardHeight, setKeyboardHeight] = useState(initialValue);

  useEffect(() => {
    const keyboardDidShow = Keyboard.addListener('keyboardDidShow', e => {
      setKeyboardHeight(e.endCoordinates.height);
    });
    const keyboardDidHide = Keyboard.addListener('keyboardDidHide', e =>
      setKeyboardHeight(e.endCoordinates.height),
    );
    return () => {
      keyboardDidShow.remove();
      keyboardDidHide.remove();
    };
  }, []);

  return keyboardHeight;
};
