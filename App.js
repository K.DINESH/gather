import React, {useEffect, useRef, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {AppContext} from './src/context/useContext';
import {HomeStack} from './src/navigation/homeStack';
import auth from '@react-native-firebase/auth';
import {
  fetchDataFromFirebase,
  findWeekNumber,
  keyboardLisner,
} from './src/utilities/fontutilities';
import LoadingIndicator from './src/components/loadingModal';

export default function App() {
  const [sortedContacts, setSortedContacts] = useState();
  const [firebaseUserDetails, setFirebaseUserDetails] = useState();
  const [userData, setUserData] = useState();
  const [isloading, setIsLoading] = useState(true);
  const [groupsDetails, setGroupsDetails] = useState([]);
  const [keyboardDetails, setKeyBoard] = useState({
    isVisible: false,
    height: null,
  });
  const [filledSpark, setFilledSpark] = useState({});
  const collegeBudsRef = useRef();
  const mobileNumbers = useRef([]);
  const thisWeekSpark = useRef({id: '', text: ''});
  const reactions = useRef({id: '', text: ''});

  const onAuthStateChanged = async user => {
    setFirebaseUserDetails(user);
    if (user) {
      const weekNumber = findWeekNumber();
      const year = new Date().getFullYear();
      const sparkId = await fetchDataFromFirebase(
        `sparks/linker/global/weekly/${year + '-' + weekNumber}`,
      );
      const sparkText = await fetchDataFromFirebase(`sparks/repo/${sparkId}`);
      thisWeekSpark.current = {id: sparkId, text: sparkText};
      reactions.current = await fetchDataFromFirebase('reaction_templates');
    }
    if (isloading) setIsLoading(false);
  };

  useEffect(() => {
    (async () => {
      mobileNumbers.current = await fetchDataFromFirebase('mobile_numbers');
    })();
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    keyboardLisner(setKeyBoard);
    return subscriber;
  }, []);

  return (
    <AppContext.Provider
      value={{
        thisWeekSpark,
        firebaseUserDetails,
        groupsDetails,
        setGroupsDetails,
        sortedContacts,
        setSortedContacts,
        keyboardDetails,
        setKeyBoard,
        collegeBudsRef,
        filledSpark,
        setFilledSpark,
        mobileNumbers,
        userData,
        setUserData,
        reactions,
      }}>
      <NavigationContainer>
        {isloading ? <LoadingIndicator visible={isloading} /> : <HomeStack />}
      </NavigationContainer>
    </AppContext.Provider>
  );
}
